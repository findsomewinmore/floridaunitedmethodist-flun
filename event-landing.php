<?php
/*
 * Template Name: Event Landing Page
 * Description: Leadership Page
 */
?>

<?php get_template_part('templates/header'); ?>


        <?php
            $leader_banner = get_field('leader_banner');
            $banner = get_field('banner');
            $navigation = get_field('navigation');
            $navigation_blurb = get_field('navigation_blurb');
            $leader_gallery = get_field('leader_gallery');
            $cta_title = get_field('cta_title');
            $cta = get_field('cta');
        ?>

<!-- Banner -->

<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>


<!-- Navigation -->

    <div class="leader-nav-section mb-1">
        <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 leader-nav about-nav">
                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => $menu );
                    wp_nav_menu( $menu_args );
                    ?>
            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 leader-blurb">
                <?php echo $navigation_blurb ?>
            </div>
        </div>

        </div>

    </div>


<!-- Leadership Section -->
    <section class="leadership-loop">
<div class="mb-2">
<div class="container">



    <div class="leader-bio-container single-leader">


              <?php $speakers = get_field('guest_speakers');

              foreach($speakers as $speaker):
                $speaker = $speaker['speaker'];

                    $leader_image = get_field('bio_image', $speaker->ID);
                    $leader_name = get_the_title($speaker->ID);

                    $leader_mini_bio = get_field('bio_text', $speaker->ID);
                    $button_url = get_field('button_url', $speaker->ID);
                    $button_text = get_field('button_text', $speaker->ID);
                    $designation = get_field('designation', $speaker->ID);
                ?>
                    <div class="bio-box" id="slide-<?php $var = sanitize_title_for_query( get_the_title($speaker->ID) ); echo esc_attr( $var);?>">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="bio-image greyscale" src="<?php echo $leader_image['url'] ?>" alt="<?php echo get_the_title($speaker->ID) ?> - Staff - <?php bloginfo( 'name' ); ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="bio-container">
                                    <h4 class="leader-name"><?php echo $leader_name ?><?php if($designation != null): echo ', ' . $designation; endif; ?></h4>


                                    <?php if( have_rows('leadership_header', $speaker->ID) ):?>
                                    <?php while ( have_rows('leadership_header', $speaker->ID) ) : the_row();

                                        $leader_title = get_sub_field('title');
                                        $leader_phone = get_sub_field('email');
                                        $leader_email = get_sub_field('phone');


                                    ?>
                                     <h5 class="leader-title"><?php echo $leader_title ?></h5>
                                    <a class="bio-email" href="mailto:<?php echo $leader_phone ?>"><?php echo $leader_phone ?></a>
                                        <a class="bio-email" href="tel:<?php echo $leader_email ?>">
                                            <?php echo $leader_email ?>
                                        </a>


                                    <?php endwhile; ?>
                                    <?php endif; ?>



                                        <span class="mini-bio"><?php echo $leader_mini_bio ?></span>
                                        <a href="<?php the_permalink($speaker->ID);?>" class="btn leader-btn">Read More
                                        </a>
                                </div>
                            </div>
                        </div>
                    </div>


              <?php endforeach;?>


                </div>


    </div>
</div>


    <div class="container directory hidden-sm-down">
        <h3><?php echo get_the_title();?></h3>
            <ul class="">

              <?php //$speakers = get_field('guest_speakers');

              foreach($speakers as $speaker):
                $speaker = $speaker['speaker'];

              $designation = get_field('designation', $speaker->ID); ?>

            <li><a href="#slide-<?php $var = sanitize_title_for_query( get_the_title($speaker->ID) ); echo esc_attr( $var);?>"><?php echo get_the_title($speaker->ID);?><?php if($designation != null): echo ', ' . $designation; endif; ?></a></li>
            <?php endforeach;?>
            </ul>


    </div>
    </section>
    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>


<?php get_template_part('templates/footer'); ?>
</div>
