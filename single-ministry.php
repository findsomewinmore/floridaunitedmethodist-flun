<?php
/**
 * The Template for displaying all single posts
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>

    <?php
        $banner = get_field('banner');
    ?>
<!-- Banner -->

<div class="main-container">

        <section class="page-banner">

<img <?php if(get_field('downloadable')): echo 'class="downloadable"'; endif;?> src="<?php if($banner !== null): echo $banner['sizes']['banner']; else: echo '' . get_template_directory_uri(). '/dist/img/flun-18.jpg'; endif;?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            <div class="overlay"></div>

            <div class="container">


                <div class="row">
                    <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

                        <h3><?php the_category(); ?></h3>
                        <h1 role="heading"><?php if(get_field('headline')): echo get_field('headline'); else: echo get_the_title(); endif;?></h1>
                        <hr class="historyhr">

                        <h5 class="text-center text-white"><?php the_field('author'); ?></h5>
                    </div>
                </div>
            </div>

            <p class="banner-caption"><?php echo $banner['caption'];?></p>

        </section>



<!-- Content Layout -->



<section>

    <div class="container">
            <div class="row">
                    <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

            <?php
                if (have_posts()) :
                   while (have_posts()) :
                      the_post();
                         the_content();
                   endwhile;
                endif;
            ;?>

            </div>

        </div>


    </div>

    </section>





<!-- Related Posts -->

<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>


</div>
