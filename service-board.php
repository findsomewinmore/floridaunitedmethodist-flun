<?php
/*
 * Template Name: Service Board
 * Description: A template for members of the board
 */
?>
<?php get_template_part('templates/header'); ?>

    <?php
        $banner = get_field('banner');

    ?>

<div class="main-container">

<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>





        <?php if( have_rows('columns') ):?>

<section class="columns">
    <div class="container">
    <div class="row">

        <?php  $counter = 0; while ( have_rows('columns') ) : the_row(); $counter++; ?>

        <?php endwhile; ?>
        <?php while ( have_rows('columns') ) : the_row(); ?>
                    <div class="col-sm-<?php echo 12 / $counter;?>">

                    <?php the_sub_field('content');?>
                    </div>
        <?php endwhile; ?>

    </div>

    </div>


</section>


        <?php endif; ?>




<section class="leadership-loop">
    <div class="mb-5">
        <div class="container">
            <div class="leader-bio-container single-leader">

                <?php $teams = get_field('service_team');


            foreach($teams as $team): ?>



                    <?php

        $board_image = get_field('bio_image', $team->ID);
        $information = get_field('bio_text', $team->ID);
    ?>
                        <div class="bio-box">
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="bio-image greyscale" src="<?php echo $board_image['sizes']['large'] ?>" />
                                </div>
                                <div class="col-md-8">
                                    <div class="bio-container">
                                        <h4 class="leader-name"><?php echo get_the_title($team->ID) ?></h4>


                                        <?php if( have_rows('leadership_header', $team->ID) ):?>
                                            <?php while ( have_rows('leadership_header', $team->ID) ) : the_row();

                                        $leader_title = get_sub_field('title');
                                        $leader_phone = get_sub_field('email');
                                        $leader_email = get_sub_field('phone');


                                    ?>
                                                <h5 class="leader-title"><?php echo $leader_title ?></h5>
                                                <a class="bio-email" href="mailto:<?php echo $leader_phone ?>">
                                                    <?php echo $leader_phone ?>
                                                </a>
                                                <a class="bio-email" href="tel:<?php echo $leader_email ?>">
                                                    <?php echo $leader_email ?>
                                                </a>


                                                <?php endwhile; ?>
                                                    <?php endif; ?>



                                                        <span class="mini-bio"><?php the_field('bio_text', $team->ID); ?></span>
                                                        <a href="<?php the_permalink($team->ID);?>" class="btn leader-btn">Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php endforeach;?>

            </div>
        </div>

    </div>
</section>



<?php get_template_part('templates/footer'); ?>
</div>
