<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

                    <?php get_template_part('templates/header'); ?>
                    <div class="main-container">
                        <section class="page-banner">

                            <img src="<?php if($banner !== null): echo $banner['sizes']['banner']; else: echo '/wp-content/uploads/Palm_trees_FUMF_Tita_Parham.jpg'; endif;?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
                                <div class="container">
                                    <div class="row">
                                    <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">
                                    <h1 role="heading"><?php echo "Search Results for: ".get_query_var('s');?></h1>

                                    </div>
                                    </div>

                                </div>

                            </section>


                    <section class="search-input">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 offset-xl-0 col-md-11 offset-md-1 col-11 offset-1">
                                        <?php get_search_form(); ?>


                                </div>

                            </div>
                        </div>
                        </section>
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2 search-body">

                                        <?php $search = get_query_var('s'); ?>

                                        <?php
                                        $swp_query = new SWP_Query(
                                            array(
                                                's' => $search,
                                                'posts_per_page' => -1, // search query
                                                'meta_query' => array(
                                                  'meta_key' => 'exclude_from_az_index',
                                                  'meta_value' => 'Yes',
                                                  'meta_compare' => '!='
                                                )
                                              )
                                          );
                                        if ( ! empty( $swp_query->posts ) ) {
                                                _e("<ul>");
                                                $i = 1;
                                            foreach( $swp_query->posts as $post ) : setup_postdata( $post ); if(!get_field('exclude_from_az_index')):?>
                                             <li>
                                                 <a href="<?php the_permalink(); ?>"><?php echo $i++;?>. <?php the_title(); ?>

                                                     <?php if(get_field('author')): $authors = get_field('author'); $authorNo = 0; foreach($authors as $author): $authorNo++; echo '<span class="author">Author: '; echo get_the_title($author->ID); echo '</span>'; endforeach; endif;?>
                                                        <?php if( have_rows('guest_authors') ):?>

                                                        <?php while ( have_rows('guest_authors') ) : the_row();?>

                                                            <span class="author">Author: <?php the_sub_field('author_name');?></span>
                                                        <?php endwhile; ?>

                                                        <?php endif; ?>

                                                     <span><?php the_permalink(); ?></span></a>

                                                 <p><?php the_excerpt();?></p>
                                                 <?php
                                                    if(!is_singular('page')):
                                                    echo '<p><b>Posted: ' . get_the_date() . '</b></p>';
                                                    endif;
                                                 ?>
                                             </li>
                                           <?php endif; endforeach;
                                    echo '</ul>';
                                    ?>

                                            <?php wp_reset_postdata();
                                        } else {
                                            ?>
                                                    <h2 style='font-weight:bold;color:#000'>Nothing Found</h2>
                                                    <div class="alert alert-info">
                                                      <?php if($search != ''){ ?>
                                                      <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
                                                    <?php } else { ?>
                                                      <p>Please enter a search query.</p>
                                                    <?php } ?>
                                                    </div><?php
                                        }?>

                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>

                    <?php get_template_part('templates/footer'); ?>

                    </div>
