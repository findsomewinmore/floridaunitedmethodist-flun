<?php
/**
 * The Template for displaying all single posts
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
?>
<!-- Banner -->

<div class="main-container">
        <section class="page-banner">
<img <?php if(get_field('downloadable')): echo 'class="downloadable"'; endif;?> src="<?php if($banner !== null): echo $banner['sizes']['banner']; else: echo '' . get_template_directory_uri(). '/dist/img/flun-18.jpg'; endif;?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            <div class="container">


                <div class="row">
                    <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

                        <h1 role="heading"><?php the_title(); ?></h1>

                    </div>
                </div>
            </div>

        </section>



    <?php
    $images = get_field('gallery');

    if( $images ): ?>
        <section>

            <div class="photo-slider center">

                <?php foreach( $images as $image ): ?>

                    <div class="photo-slider__item">
                        <img src="<?php echo $image['url']; ?>" />
                        <p>
                            <?php echo $image['caption'];?>
                        </p>
                    </div>

                    <?php endforeach; ?>
            </div>

        </section>

    <?php endif;?>



<section class="">

    <div class="container">

        <div class="row">

            <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

                <div class="post-body">

                    <div class="event-info">
                        <h4><?php echo get_the_title();?></h4>
                        <p>
                            <?php the_field('location'); ?>
                        </p>
                    </div>

                    <?php
                if (have_posts()) :
                   while (have_posts()) :
                      the_post();
                         the_content();
                   endwhile;
                endif;
            ;?>
                 <?php the_field('script');?>
                </div>



            </div>
        </div>

    </div>




</section>



<section class="section-header">
    <div class="container">
        <div class="row">

            <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

                <h2 class="back-dash">Upcoming Events</h2>

            </div>
        </div>
    </div>
</section>

    <section class="news_section p0">




<?php $args = array(

    'posts_per_page' => 1,
    'orderby' => 'date' ,
    'order'   => 'DESC',
    'post_type' => 'eventlists');

$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>


<?php endwhile; wp_reset_postdata();?>

            <?php
                $date = get_the_date();
                $title = get_the_title();
                $news_blurb = get_the_excerpt();
                $post_url = get_the_permalink();
                // $link_text = $news_items['link_text'];
                $news_image = get_field('banner');
            ?>

                <div class="news" style="background-image: url(<?php echo $banner['sizes']['large']; ?>);">
                    <div class="container">
                        <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2 news-container">

                            <h2 class="news_title"><?php echo $title ?></h2>
                            <p class = "news-blurb"><?php echo $news_blurb ?></p>

                            <a class="news-btn btn" href="<?php echo $post_url ?>">Read More</a>
                        </div>
                    </div>
                </div>



</section>


<div class="share-bar">
    <p>Share</p>
    <ul>
        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/home?status=<?php the_permalink();?>"><i class="fa fa-twitter"></i></a></li>
        <li><a href="https://plus.google.com/share?url=<?php the_permalink();?>"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>"><i class="fa fa-linkedin"></i></a></li>
        <li><a href="mailto:?subject=<?php echo get_the_title();?>&body=Check%20out%20this%20link%20<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/envelope.svg"></a></li>
    </ul>

</div>

<?php get_template_part('templates/footer'); ?>

        </div>

