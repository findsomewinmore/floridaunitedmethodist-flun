<?php
/*
 * Template Name: Single Team Members
 Template Post Type: team_members
 *
 */
?>


<?php get_template_part('templates/header'); ?>



        <?php
            $single_banner = get_field('single_banner');
            $leadership_header = get_field('leadership_header');
            $bio_image = get_field('bio_image');
            $bio_text = get_field('bio_text');
            $carousel = get_field('carousel');
            $cta_title = get_field('cta_title');
            $cta = get_field('cta');
        ?>


<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
        <div class="overlay"></div>
            <div class="container">
                <div class="row">
                                <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">
                <?php foreach($leadership_header as $key => $single_header){ ?>
                    <?php
                        $name = $single_header['name'];
                        $title = $single_header['title'];
                        $email = $single_header['email'];
                        $phone = $single_header['phone'];
                    ?>
                <h1 class="single-info"><?php echo get_the_title(); ?></h1>
                <hr class="leader-line">
                <p class="single-title text-center text-white"><?php echo $title ?></p>
                <p class="email-phone text-center text-white"><?php echo $email ?> &nbsp; <?php echo $phone ?></p>
                <?php } ?>
                    </div>
                </div>

            </div>

        </section>

    <section>

    <div class="container">

        <div class="leader-bio">
            <div class="row">
                <div class="col-md-4">
                    <img class="single-bio-image greyscale" src="<?php echo $bio_image['url'] ?>" />
                </div>
                <div class="col-md-8 bio-text">
                    <?php echo $bio_text ?>
                    <a href="#back" onclick="window.history.go(-1)" class="btn mt-5"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </div>
        </div>


    </div>

</section>


<!-- Carousel Section -->
    <section>





    <?php
    $images = get_field('leaders');

    if( $images ): ?>

         <div class="photo-slider center">

        <?php foreach( $images as $image ): ?>

             <div class="photo-slider__item">
                 <img src="<?php echo $image['url']; ?>" />
                 <div>
                                  <p><?php echo $image['caption'];?></p>

                 </div>
             </div>

         <?php endforeach; ?>
     </div>
    <?php endif;?>





    </section>

    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>

    </div>


<?php get_template_part('templates/footer'); ?>
