<?php
/**
 * Template Name: Galleries
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

    <?php get_template_part('templates/header'); ?>




        <div class="main-container">
            <section class="page-banner">

                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" />
                <div class="overlay"></div>
                <div class="container">

                    <h1 class="single-info"><?php echo get_the_title();?></h1>
                </div>

            </section>
            <section class="pb-0">

                <div class="container mb-5">
                    <div class="row">
                        <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">

                            <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                            <?php //echo do_shortcode('[fumf-childpages]');?>

                                <?php $menu = get_field('page_menu');?>

                                    <?php
                    $menu_args = array('menu' => 39 );
                    wp_nav_menu( $menu_args );
                    ?>

                        </div>
                        <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0">

                            <blockquote>Photos provided in this gallery are available for editorial, noncommercial uses only. Any photos used must include the source and photographer attribution: FUMF photo by photographer’s name at www.fumf.org.</blockquote>
                            <?php $taxonomy     = 'category';
                                    $orderby      = 'name';
                                    $show_count   = false;
                                    $pad_counts   = false;
                                    $hierarchical = true;
                                    $title        = '';

                                    $args = array(
                                        'taxonomy'     => 'media_category',
                                        'orderby'      => $orderby,
                                        'show_count'   => $show_count,
                                        'pad_counts'   => $pad_counts,
                                        'hierarchical' => $hierarchical,
                                        'title_li'     => $title,
                                        'meta_query' => array(
                                            array(
                                                'key' => 'exclude_public',
                                                'compare' => '==',
                                                'value' => '1'
                                            )
                                        )
                                    );
                                    ?>

                                <ul class="media-cat">
                                    <?php wp_list_categories( $args ); ?>
                                </ul>

                        </div>
                    </div>
                </div>


            </section>

        </div>

        <?php include( locate_template( 'partials/parts/recent-news.php', false, false ) );?>
            <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
                <?php get_template_part('templates/footer'); ?>
