<?php
/**
 * Template Name: Videos
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>




<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" />
        <div class="overlay"></div>
            <div class="container">

                <h1 class="single-info">Videos</h1>
            </div>

        </section>

    <section>
        <div class="container">

            <div class="row">



            <?php
                  // set up or arguments for our custom query
                  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                  $query_args = array(
                    'post_type' => 'videos',
                    'posts_per_page' => 12,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged,
                    'post_status' => 'publish'
                  );
                  // create a new instance of WP_Query
                  $the_query = new WP_Query( $query_args );
                ?>

<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>

                    <?php $video = get_field('url', false, false);

                $id = substr($video, strrpos($video, '/') + 1);

                ?>

                                <a href="<?php the_permalink();?>" class="col-md-4 gallery-item video">
                                    <?php if(get_field('poster')):?>
                                    <img src="<?php echo get_field('poster')['sizes']['large'];?>">
                                    <?php else:?>
                                    <img src="https://img.youtube.com/vi/<?php echo $id;?>/hqdefault.jpg">

                                    <?php endif;?>
                                    <p><?php echo get_the_title();?></p>
                                </a>


        <?php endwhile; ?>



            </div>
                    <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                    <div class="row">


                        <div class="col-sm-6">
                            <div class="nav-next alignright">
                                <?php echo get_next_posts_link( 'Older videos', $the_query->max_num_pages ); // display older posts link ?>
                            </div>

                        </div>
                        <div class="col-sm-6">

                            <div class="nav-previous alignleft">
                                <?php echo get_previous_posts_link( 'Newer videos' ); // display newer posts link ?>
                            </div>

                        </div>
                    </div>

                    <?php } ?>

                <?php endif;?>
        </div>

    </section>

</div>

<?php include( locate_template( 'partials/parts/recent-news.php', false, false ) );?>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>
