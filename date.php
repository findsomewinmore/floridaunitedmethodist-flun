<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>




<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" />
        <div class="overlay"></div>
            <div class="container">

                <h1 class="single-info"><?php the_archive_title(); ?></h1>
            <div class="date-archive">
                <div class="row">
                    <div class="col-sm-4 offset-sm-2">
                      <?php echo '<span style="color: #FFF">Select a month</span>'; ?>
                <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                    <option value="">
                        <?php esc_attr( _e( 'Month', 'textdomain' ) ); ?>
                    </option>
                    <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
                </select>
                    </div>
                    <div class="col-sm-4">
                      <?php echo '<span style="color: #FFF">Select a category</span>'; ?>
                <?php wp_dropdown_categories( 'hide_empty=0&exclude=6,7,8,13,57' ); ?>
                <script type="text/javascript">

                    var dropdown = document.getElementById("cat");
                    function onCatChange() {
                        if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                        }
                    }
                    dropdown.onchange = onCatChange;

                </script>


                    </div>

                </div>

            </div>


            </div>

        </section>



                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>

            <?php
                $date = get_the_date();
                $title = get_the_title();
                $news_blurb = get_the_excerpt();
                $post_url = get_the_permalink();
                if(get_field('preview_image')): $banner = get_field('preview_image'); else: $banner = get_field('banner'); endif;
            ?>

                 <div class="news" style="background-image: url(<?php echo $banner['sizes']['banner']; ?>);">

                    <div class="container">

                        <div class="news-container col-md-10">
                            <h1 class="news_title"><?php echo $title ?></h1>
                            <p class = "news-blurb"><?php echo $news_blurb ?></p>
                            <a class="news-btn btn" href="<?php echo $post_url ?>">Read More</a>
                        </div>
                    </div>
                </div>

        <?php endwhile; endif; ?>


    <section class="pagination">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>

                </div>
                <div class="col-sm-6">

                <div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>

                </div>

            </div>

        </div>


    </section>

</div>

<?php get_template_part('templates/footer'); ?>
