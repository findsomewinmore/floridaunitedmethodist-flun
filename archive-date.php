<?php
/**
 * Template Name: Date Archive
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>




<div class="main-container">
 <section class="page-banner">

  <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" />
  <div class="overlay"></div>
  <div class="container">

   <h1 class="single-info">
    <?php echo get_the_title(); ?>
   </h1>
   <div class="date-archive">
    <div class="row">
     <div class="col-sm-4 offset-sm-2">
      <?php echo '<span style="color: #FFF">Select a Month</span>'; ?>
      <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
       <option value="">
        <?php esc_attr( _e( 'Month', 'textdomain' ) ); ?>
       </option>
       <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
      </select>
     </div>
     <div class="col-sm-4">
      <?php echo '<span style="color: #FFF">Select a Category</span>'; ?>
      <?php wp_dropdown_categories( 'hide_empty=0&exclude=6,7,8,13,57' ); ?>
      <script type="text/javascript">
       var dropdown = document.getElementById("cat");

       function onCatChange() {
        if (dropdown.options[dropdown.selectedIndex].value > 0) {
         location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat=" + dropdown.options[dropdown.selectedIndex].value;
        }
       }
       dropdown.onchange = onCatChange;

      </script>


     </div>

    </div>

   </div>
  </div>

 </section>





 <section class="">
  <div class="container">

   <div class="row">
    <div class="col-sm-12">

     <ul class="date-archives">

      <?php wp_get_archives( array( 'type' => 'monthly', 'wrap' => 'li', 'show_post_count' => 1 ) ); ?>

     </ul>

    </div>

   </div>

  </div>


 </section>

</div>

<?php get_template_part('templates/footer'); ?>
