<?php
/**
 * Functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package Smores
 * @since Smores 2.0
 */

define("THEME_ROOT", get_stylesheet_directory());

// Composer dependencies
require_once __DIR__ . '/lib/vendor/autoload.php';

use Smores\Smores;
use Smores\TopBarPageWalker;
use Smores\TopBarWalker;

$rand = rand();

$smores = new Smores(
    array( // Includes
        'lib/admin',         // Add admin scripts
        'lib/ajax',          // Add ajax scripts
        'lib/classes',       // Add classes
        'lib/custom-fields', // Add custom field scripts
        'lib/forms',         // Add form scripts
        'lib/images',        // Add images scripts
        'lib/post-types',    // Add post type scripts
        'lib/shortcodes',    // Add shortcode scripts
        'lib/widgets',       // Add widget scripts
    ),
    array( // Assets
        'css'             => '/dist/css/styles.min.css',
        // 'slickscss'        => '/src/scss/slick.scss',
        // 'slicktheme'      => '/src/scss/slick-theme.scss',
//        'css'             => '/dist/css/styles.min.clean.css',
        'js'              => '/dist/js/scripts.min.js?rand=' . $rand,
        'modernizr'       => '/dist/js/vendor/modernizr.min.js?rand=' . $rand,
        'jquery'          => '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
        'jquery_fallback' => '/dist/js/vendor/jquery.min.js',
        // 'slickjs'         => '/src/js/slick.min.js',
        // 'slick-init'      => '/src/js/slick-init.js',
)
);

/**
 * [smores_numeric_pagination description]
 *
 * @param  [type] $custom_query [description]
 * @param  string $classes      [description]
 * @return [type]               [description]
 */
function smores_numeric_pagination($custom_query = false, $classes = '')
{
    $query = null;

    if ($custom_query) {
        $query = $custom_query;
    } else {
        global $wp_query;

        $query = $wp_query;

        if (is_singular()) {
            return;
        }
    }

    /** Stop execution if there's only 1 page */
    if ($query->max_num_pages <= 1) {
        return;
    }

    $paged = (get_query_var('paged')) ? absint( get_query_var('paged')) : 1;
    $max   = $query->max_num_pages;

    /** Add current page to the array */
    if ($paged >= 1) {
        $links[] = $paged;
    }

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo "<ul class=\"pagination {$classes}\" role=\"menubar\" aria-label=\"Pagination\">\n";

    /** Previous Post Link */
    if (get_previous_posts_link()) {
        printf("<li class=\"arrow show-for-medium-up previous-link\">%s</li>\n", get_previous_posts_link());
    } else {
        echo '<li class="arrow unavailable previous-link"><a href="#">&laquo; Previous Page</a></li>';
    }

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = ($paged === 1) ? ' class="current"' : '';

        printf("<li%s><a href=\"%s\">%s</a></li>\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links)) {
            echo '<li>…</li>';
        }
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);

    foreach ((array) $links as $link) {
        $class = ($paged === $link) ? ' class="current"' : '';

        printf("<li%s><a href=\"%s\">%s</a></li>\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links)) {
            echo "<li>…</li>\n";
        }

        $class = ($paged === $max) ? ' class="current"' : '';

        printf("<li%s><a href=\"%s\">%s</a></li>\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link()) {
        printf("<li class=\"arrow next-link\">%s</li>\n", get_next_posts_link());
    } else {
        echo '<li class="arrow unavailable show-for-medium-up next-link"><a href="#">Next Page &raquo;</a></li>';
    }

    echo "</ul>\n";
}


/////Register Nav Menus
function theme_register_menus() {
    register_nav_menus(
        array(
        'primary-menu' => __( 'Primary Menu', 'text-domain' ),
        'footer-menu' => __( 'Footer Menu', 'text-domain' ),
    )
    );
}
add_action( 'init', 'theme_register_menus' );

//function my_searchwp_add_weight_to_newer_posts( $sql ) {
//    global $wpdb;
//    // Adjust how much weight is given to newer publish dates. There
//    // is no science here as it correlates with the other weights
//    // in your engine configuration, and the content of your site.
//    // Experiment until results are returned as you want them to be.
//    $modifier = 100;
//    $sql .= " + ( 100 * EXP( ( 1 - ABS( ( UNIX_TIMESTAMP( {$wpdb->prefix}posts.post_date ) - UNIX_TIMESTAMP( NOW() ) ) / 86400 ) ) / 1 ) * {$modifier} )";
//    return $sql;
//}
//add_filter( 'searchwp_weight_mods', 'my_searchwp_add_weight_to_newer_posts' );

// ALWAYS return search results ordered by date
//add_filter( 'searchwp_return_orderby_date', '__return_true' );

function my_searchwp_return_orderby_date( $order_by_date, $engine ) {

    // for the Default engine (only!) we want to return search results by date
    if ( 'default' == $engine ) {
        $order_by_date = true;
    }

    return $order_by_date;
}
add_filter( 'searchwp_return_orderby_date', 'my_searchwp_return_orderby_date', 10, 2 );

function searchwp_term_highlight_auto_excerpt( $excerpt ) {
    global $post;
    if ( ! is_search() ) {
        return $excerpt;
    }
    // prevent recursion
    remove_filter( 'get_the_excerpt', 'searchwp_term_highlight_auto_excerpt' );
    $global_excerpt = searchwp_term_highlight_get_the_excerpt_global( $post->ID, null, get_search_query() );
    add_filter( 'get_the_excerpt', 'searchwp_term_highlight_auto_excerpt' );
    return wp_kses_post( $global_excerpt );
}
add_filter( 'get_the_excerpt', 'searchwp_term_highlight_auto_excerpt' );


/////Add Options Page
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array('page_title' => 'Site Options', 'icon_url' =>'dashicons-admin-generic','position' => '2'));
    acf_add_options_page(array('page_title' => 'Resource List', 'icon_url' =>'dashicons-media-text','position' => '3'));
}



/**
*	Smores Included Plugin Installs
*   Advanced Custom Fields PRO
*   Advanced Custom Fields: Font Awesome
*   Advanced Custom Fields: Contact Form 7
*   Contact Form 7
*   EWWW Image Optimizer
*   Grunt Sitemap Generator
*   Regenerate Thumbnails
*   WP Security Audit Log
**/

add_action('after_switch_theme' , 'smores_install_plugin_pack');

// First check it make sure this function has not been done
function smores_install_plugin_pack(){
//    if (get_option('smores_plugin_installer_ran') != "yes") {
//
//        smores_run_install_plugin_pack();
//
//    // Now that function has been run, set option so it wont run again
//        update_option( 'smores_plugin_installer_ran', 'yes' );
//
//    }
    smores_run_install_plugin_pack();
}

add_action('switch_theme', 'smores_reset_installer_switch');
function smores_reset_installer_switch(){
    if(get_option('smores_plugin_installer_ran') == "yes"){
       // update_option( 'smores_plugin_installer_ran', 'no');
    }
}

function smores_run_install_plugin_pack(){
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
    require_once ABSPATH . 'wp-admin/includes/file.php';
    $zippedThemePlugins = get_template_directory() . '/smores_pu/plugins/';
    $pluginsDirectory =  get_home_path() . 'wp-content/plugins/';

    // smores_check_for_and_install_plugins( $zippedThemePlugins, $pluginsDirectory );
    //smores_plugin_directory_scanner( $pluginsDirectory, $activate = true );
    // smores_clean_tmp_directory( $zippedThemePlugins . 'tmp' );

    return true;
}



/**
 * Checks for installed plugins. If not previously installed. Installs and activates plugin.
 * @param  string $zippedPath           Path to zipped plugins required for the theme.
 * @param  string $installedPluginsPath Path to currently installed plugins.
 * @return bool                       true if function complets.
 */
function smores_check_for_and_install_plugins( $zippedPath, $installedPluginsPath ){
    $alreadyInstalled = array();
    $tmpPlugins = array();
    $tmpUnzipPath = $zippedPath . 'tmp/';


    if( !file_exists($tmpUnzipPath) ){
        mkdir( $tmpUnzipPath , 0774 );
    }

    if( file_exists( $tmpUnzipPath ) == false ){
        echo "ERROR, tmp directory does not exist!"; //This needs to be dealt with properly later return an error
    } else {
        smores_unpack_plugins($zippedPath , $tmpUnzipPath);

        $tmpPlugins = smores_plugin_directory_scanner($tmpUnzipPath , $activate = false);
        $alreadyInstalled = smores_plugin_directory_scanner($installedPluginsPath , $activate = false);
    }

    foreach ($tmpPlugins as $tmpPlugin) {
        $i = 1;

        if(sizeof($alreadyInstalled) == 0 ){
            $alreadyInstalled[] = array('Name' => null , 'FilePath' => null , 'DirectoryPath' => null);
        }

        foreach ($alreadyInstalled as $installedPlugin) {
            if( $installedPlugin['Name'] == $tmpPlugin['Name'] ){

                //Do Nothing
                break;

            } else {

                if($i == sizeof($alreadyInstalled)){
                    $getDirectoryToCopy = explode('/', $tmpPlugin['DirectoryPath']);
                    $directoryToCopy = array_pop($getDirectoryToCopy);
                    $fileToCopyPath = explode('/', $tmpPlugin['FilePath']);
                    $fileToCopy = '/' . array_pop($fileToCopyPath);
                    $newCompleteDirectory = $installedPluginsPath . $directoryToCopy;

                    if(!file_exists( $newCompleteDirectory)){
                        mkdir( $newCompleteDirectory);
                    }

                    smores_recursive_copy($tmpPlugin['DirectoryPath'] ,  $newCompleteDirectory);

                    $activateMe[] = $installedPluginsPath . $directoryToCopy . $fileToCopy;
                }
            }

            $i++;

        }
    }

    $pluginsToActivate = sizeof($activateMe);
    for($i=0; $i<$pluginsToActivate; $i++){
        activate_plugin($activateMe[$i]);
    }

    unset($tmpUnzipPath);
    return true;
}

/**
 * Scans the given directory for .zip files.
 * Extracts zips to specified directory
 *
 * @param string $zippedPath	Path to directory with zip files.
 * @param string $unzippedPath	Path to unzip files to.
 * @return bool true
 */
function smores_unpack_plugins( $zippedPath , $unzippedPath ){

    foreach ( scandir($zippedPath) as $zippedPluginFile ) {
        $zip = new ZipArchive();

        if ( substr($zippedPluginFile, -4) == '.zip' ){
            $zip->open($zippedPath . $zippedPluginFile );
            $zip->extractTo( $unzippedPath);
            //$zip->close; add if exists for close() method
            unset($zip);
        }
    }

    return true;
}


/**
 * Scans a given directory for php files which contain WP plugin info.
 * @param  string  $dir      Wordpress Plugins directory usually /wp-content/plugins
 * @param  boolean $activate When set to true found plugins are activated.
 * @return array           returns a multi-dementional array containing plugin Name, php file path and directory path
 */
function smores_plugin_directory_scanner($dir , $activate = false){
    $pluginInfoArray = array();

    foreach (scandir($dir) as $directoryContent) {

        if(is_dir($dir . $directoryContent)){

            if($directoryContent == '.' || $directoryContent == '..'){
                //do nothing

            } else {

                foreach (scandir($dir . $directoryContent) as $file) {

                    if (substr($file, -4) == '.php'){

                        $pluginDirectoryPath =  $dir . $directoryContent;
                        $fullPluginPath = $dir . $directoryContent  .'/'.  $file;

                        $pluginInfo =  get_plugin_data($fullPluginPath);

                        if(strlen($pluginInfo['Name']) > 0 ){
                            $pluginInfoArray[] = array('Name' => $pluginInfo['Name'] , 'FilePath' => $fullPluginPath , 'DirectoryPath' => $pluginDirectoryPath);
                            //echo $pluginInfoArray['Name'] . '<- Name<br>';
                            if($activate == true){
                                activate_plugin($fullPluginPath);
                            }
                        }

                    }

                    unset($pluginInfo);
                    unset($newActivePlugin);
                }
            }
        }
    }

    return $pluginInfoArray;

}


/**
 * Recursivly copies directories and contents to a new location.
 * @param  string  $src   Path to the source directory.
 * @param  string  $dst   Path to the destination directory.
 * @param  integer $depth A safty to avoid infinte loops.
 * @return bool true;
 */
function smores_recursive_copy( $src , $dst, $depth = 1000 ){
    global $loopStop;
    $loopStop++;
    if($loopStop > $depth){
        return;
    } else {
        $files = glob($src . "/*");
        foreach($files as $file){
            if($file != '.' || $file != '..'){
                if(is_dir($file)){
                    $fileName = explode('/' , $file);
                    $newFile = array_pop($fileName);
                    $newPath = $dst . '/' . $newFile;
                    if(!file_exists($newPath)){
                        mkdir($dst . '/' . $newFile, 0775 );
                    }

                    $newSrc = $src . '/' . $newFile;
                    $newDst = $dst . '/' . $newFile;
                    smores_recursive_copy($newSrc, $newDst);
                } else {
                    $fileName = explode('/' , $file);
                    $newFile = array_pop($fileName);
                    copy($file , $dst . '/' . $newFile);
                }
            }
        }
    }

    return true;

}

function smores_clean_tmp_directory($zippedThemePlugins){

    foreach (scandir( $zippedThemePlugins ) as $file){
        if ($file == '.' || $file == '..'){
            //do nothing
        } else {
            if (is_dir( $zippedThemePlugins . '/' . $file )){
                $newDirectoryPath = $zippedThemePlugins . '/' .$file;
                smores_clean_tmp_directory( $newDirectoryPath );
            } else {
                unlink( $zippedThemePlugins . '/' . $file );
            }
        }
    }

    rmdir( $zippedThemePlugins );

}

/**
*	END Smores Included Plugin Installs
*   ACF Pro
*   ACF Font Awesome
*   ACF CF7
*   Contact Form 7
*   EWWW Image Optimizer
*   Regenerate Thumbnails
*   WP Security Audit Log
**/



/** Smores Admin Theme Function **/
if(!function_exists('add_favicon')) {
function add_favicon() {
      $favicon_url = get_template_directory_uri() . '/admin/img/favicon.ico';
    echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

// Now, just make sure that function runs when you're on the login page and admin pages
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
// add_action( 'wp_enqueue_scripts', 'fiwi_slick_enqueue_scripts_styles' );
}


function fiwi_admin_styles2() {
    wp_enqueue_style( 'admin', get_template_directory_uri() . '/admin/css/styles.min.css', false, 'all' );
    wp_enqueue_script( 'admin', get_template_directory_uri() . '/admin/js/scripts.js', array( 'jquery' ), 'all' );
}

add_action( 'admin_enqueue_scripts', 'fiwi_admin_styles2' );
add_action( 'login_enqueue_scripts', 'fiwi_admin_styles2' );

//$counter = 0;
//function fiwi_admin_fonts() {
//
//if( have_rows('font','options') ):
//while ( have_rows('font','options') ) : the_row();
//
//wp_enqueue_style( 'font', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700', false, 'all' );
//
//endwhile; endif;
//
//}
//
//add_action( 'admin_enqueue_scripts', 'fiwi_admin_fonts' );
//add_action( 'login_enqueue_scripts', 'fiwi_admin_fonts' );

function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_welcome', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_meta' );

add_action('init', 'oldfashioned_admin_settings');
function oldfashioned_admin_settings() {
    if (function_exists('acf_add_options_page')) {
        $page = acf_add_options_page(array(
            'menu_title' => 'Admin Settings',
            'menu_slug' => 'admin-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ));
    }

}

//acf

add_action('init', 'acf_fields_admin');
function acf_fields_admin() {
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_58921d3dedafd',
    'title' => 'FIWI Admin Settings',
    'fields' => array (
        array (
            'placement' => 'top',
            'endpoint' => 0,
            'key' => 'field_58921d6f0db2f',
            'label' => 'WP Admin Bar',
            'name' => '',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
        ),
        array (
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
            'key' => 'field_58921d490db2d',
            'label' => 'Admin Bar Logo',
            'name' => 'admin_bar_logo',
            'type' => 'image',
            'instructions' => 'Transparent White 1:1 Logo Works Best',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
        ),
        array (
            'placement' => 'top',
            'endpoint' => 0,
            'key' => 'field_58921d7c0db30',
            'label' => 'WP Login',
            'name' => '',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
        ),
        array (
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
            'key' => 'field_58921d870db31',
            'label' => 'WP Login Logo',
            'name' => 'wp_login_logo',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
        ),

    ),
    'location' => array (
        array (
            array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'admin-settings',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'seamless',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;
}

if(!function_exists('client_logos')) {
add_action( 'admin_head', 'client_logos' );
add_action( 'login_head', 'client_logos' );
function client_logos() {
    include( 'admin/css/style.php' );
}

}

if(!function_exists('remove_menus')) {
function remove_menus(){

//  remove_menu_page( 'index.php' );                  //Dashboard
//  remove_menu_page( 'jetpack' );                    //Jetpack*
  // remove_menu_page( 'edit.php' );                   //Posts
//  remove_menu_page( 'upload.php' );                 //Media
//  remove_menu_page( 'edit.php?post_type=page' );    //Pages
//  remove_menu_page( 'edit-comments.php' );          //Comments
//  remove_menu_page( 'themes.php' );                 //Appearance
//  remove_menu_page( 'plugins.php' );                //Plugins
//  remove_menu_page( 'users.php' );                  //Users
//  remove_menu_page( 'tools.php' );                  //Tools
//  remove_menu_page( 'options-general.php' );        //Settings

}
add_action( 'admin_menu', 'remove_menus' );
}


if(!function_exists('fix_svg')) {
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

global $wp_version; if( $wp_version == '4.7' || ( (float) $wp_version < 4.7 ) ) { return $data; }

$filetype = wp_check_filetype( $filename, $mimes );

return [ 'ext' => $filetype['ext'], 'type' => $filetype['type'], 'proper_filename' => $data['proper_filename'] ];

}, 10, 4 );

function cc_mime_types( $mimes ){ $mimes['svg'] = 'image/svg+xml'; return $mimes; } add_filter( 'upload_mimes', 'cc_mime_types' );

function fix_svg() { echo '<style> .attachment-266×266, .thumbnail img { width: 100% !important; height: auto !important; } </style>'; } add_action( 'admin_head', 'fix_svg' );
}
add_filter('get_user_option_admin_color', 'change_admin_color');
function change_admin_color($result) {
return 'ocean';
}

require_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
if (file_exists(WP_PLUGIN_DIR . '/hello.php'))
  delete_plugins(array('hello.php'));


$post = get_page_by_path('hello-world',OBJECT,'post');
if ($post)
  wp_delete_post($post->ID,true);


$post = get_page_by_path('sample-page',OBJECT,'page');
if ($post)
  wp_delete_post($post->ID,true);


if(!function_exists('change_howdy')) {
function change_howdy($translated, $text, $domain) {

    if (!is_admin() || 'default' != $domain)
        return str_replace('Howdy', 'Hey 👋', $translated);

    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'Hey 👋', $translated);

    return $translated;
}
add_filter('gettext', 'change_howdy', 10, 3);
}

function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyCHq25aMLGrFeaiHJRKxnirT6f91LzAnxM');
}

add_action('acf/init', 'my_acf_init');


// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');


// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'Button',
            'selector' => 'a',
            'classes' => 'btn'
        ),
        array(
            'title' => 'Serif Font',
            'selector' => 'h1, h2, h3, h4, h5',
            'classes' => 'serif'
        ),
        array(
            'title' => 'Caption',
            'selector' => 'p',
            'classes' => 'caption'
        ),
        array(
            'title' => 'Disclaimer',
            'selector' => 'p',
            'classes' => 'disclaimer'
        ),
        array(
            'title'     => 'Banner Heading 1',
            'selector'  => 'h1',
            'classes'   => 'banner-h1'
        ),
        array(
            'title'     => 'Banner Heading 3',
            'selector'  => 'h3',
            'classes'   => 'banner-h3'
        ),
        array(
            'title'     => 'Video - Align Left',
            'selector'  => 'div',
            'classes'   => 'video-left'
        ),
        array(
            'title'     => 'Disable Download',
            'selector'  => 'img, figure',
            'classes'   => 'no-download'
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


add_image_size( 'banner',1800,9999999999999 );
add_image_size( 'medium_large',1170,9999999999999 );

//function custom_field_excerpt() {
//    global $post;
//    $text = get_field('bio_text'); //Replace 'your_field_name'
//    if ( '' != $text ) {
//        $text = strip_shortcodes( $text );
//        $text = apply_filters('the_content', $text);
//        $text = str_replace(']]&gt;', ']]&gt;', $text);
//        $excerpt_length = 100; // 20 words
//        $excerpt_more = apply_filters('excerpt_more', ' ' . '...');
//        $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
//    }
//    return apply_filters('the_excerpt', $text);
//}

function posts_orderby_lastname ($orderby_statement)
{
  $orderby_statement = "RIGHT(post_title, LOCATE(' ', REVERSE(post_title)) - 1) ASC";
    return $orderby_statement;
}

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="btn"';
}


function wpdocs_theme_add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/admin/css/styles.min.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


function _remove_script_version( $src ){
$parts = explode( '?ver', $src );
return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


//function custom_excerpt_length( $length = 20 ) {
//   return $length;
//}
//add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
//
//
//function custom_excerpt( $length = 55 ) {
//    if( $post->post_excerpt ) {
//        $content = get_the_excerpt();
//    } else {
//        $content = get_the_content();
//        $content = wp_trim_words( $content , $length );
//    }
//    return $excerpt;
//}


function fumf_list_child_pages() {

global $post;

if ( is_page() && $post->post_parent )

    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
else
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );

if ( $childpages ) {

    $string = '<ul>' . $childpages . '</ul>';
}

return $string;

}

add_shortcode('fumf-childpages', 'fumf_list_child_pages');

function myplugin_settings() {
// Add tag metabox to page
//register_taxonomy_for_object_type('post_tag', 'page');
// Add category metabox to page
register_taxonomy_for_object_type('category', 'page');
}
 // Add to the admin_init hook of your theme functions.php file
add_action( 'init', 'myplugin_settings' );


function flun_notification_logo()
{

  $html = '<img class="size-medium wp-image-3981" style="margin: 0;" src="' .site_url() . '/wp-content/uploads/FUMF-Color-Signature-Logo-2014-600-300x140.png" alt="" width="300" height="140" />';
  return $html;
}
add_shortcode( 'email-logo', 'flun_notification_logo');


add_shortcode( 'resource-link', 'resource_link' );
function resource_link( $atts ) {
    ob_start();

    $atts = shortcode_atts(
        array(
            'button-label' => '',
            'id' => ''
        ),
        $atts,
        'resource-link'
    );

?>



        <p class="">
            <a href="<?php the_field('file', esc_attr($atts['id']));?>" class="btn downloadable" target="_blank"><i class="fa fa-file-pdf-o"></i> <?php echo esc_attr($atts['button-label']);?></a>
        </p>

        <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }

add_shortcode( 'resource-link-only', 'resource_link_only' );

function resource_link_only( $atts ) {
    ob_start();

    $atts = shortcode_atts(
        array(
            'link-text' => '',
            'id' => ''
        ),
        $atts,
        'resource-link-only'
    );

?><a href="<?php the_field('file', esc_attr($atts['id']));?>" class="downloadable" target="_blank"><?php echo esc_attr($atts['link-text']);?></a><?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }


add_shortcode( 'YT-video', 'yt_video' );

function yt_video( $atts ) {
    ob_start();

    $atts = shortcode_atts(
        array(
            'alignment' => '',
            'id' => '',
            'caption' => ''
        ),
        $atts,
        'YT-video'
    );

?>

        <div class="wp-caption <?php echo esc_attr($atts['alignment']);?> video">
            <div class="iframe-wrapper ">
            <iframe src="https://www.youtube.com/embed/<?php echo esc_attr($atts['id']);?>?feature=oembed" frameborder="0" gesture="media" allowfullscreen=""></iframe>

            </div>
            <div class="mt-1">
            <p><?php echo esc_attr($atts['caption']);?></p>
            </div>
        </div>


<?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }

// Row
function my_row( $atts, $content ) {
      ob_start();
	$content = preg_replace( "/\[\/column\](\<br \/\>|\<\/p\>.?\<p\>).?\[column/s", '[/column][column', $content );

   	return '<div class="row">' . do_shortcode( $content ) . '</div>';

       return ob_get_clean();
        
}
function my_hr() {
    ob_start();
  
     return '<div style="background-color: #6dc05d; width: 100%; height: 2px; margin-bottom: 1rem;"></div>';

  return ob_get_clean();      
}

add_shortcode( 'green-hr', 'my_hr' );

//   Columns
function my_column( $atts, $content ) {
    ob_start();
    $atts = shortcode_atts( array(
        'small' => 12,
        'medium' => 6,
        'large' => 6
    ), $atts );
    
    $atts['medium'] = ( $atts['medium'] == null ) ? $atts['small'] : $atts['medium'];
    $atts['large'] = ( $atts['large'] == null ) ? $atts['medium'] : $atts['large'];
   
    extract( $atts );
    
    $sizes = 'col-sm-' . $small . ' col-md-' . $medium . ' col-lg-' . $large;
    
    return '<div class="sc_columns ' . $sizes . '">' . do_shortcode( $content ) . '</div>';

    return ob_get_clean();
    
}

add_shortcode( 'column', 'my_column');





add_action('admin_footer', 'resource_action');
function resource_action() { ?>



    <script>
//        jQuery('select').on('select2:select', function(evt){
//
//            console.log(jQuery(this).val());
//
//            var id = jQuery(this).val();
//            var idLast = id.slice(-1)[0];
//
//
//            jQuery('.acf-field-599330ca98b59 .acf-input').append('[resource-link id="' + idLast + '" button-label=""]<br>');
//            jQuery('.acf-field-599330ca98b59 .acf-input').append('[resource-link-only id="' + idLast + '" link-text=""]<br>');
//
//
//        })



</script>

                <div id="resource-link" style="display: none;">
                    <div class="title acf-soh">

                        <div class="title-value">
                            <h1>Add Resource Link</h1>
                            <h2>Link Format</h2>
                            <div id="link-attr">
                                <label>Button
                                    <input type="radio" name="type" value="resource-link" class="link-type">
                                </label>
                                <label>Link Only
                                    <input type="radio" name="type" value="resource-link-only" class="link-type">
                                </label>
                                <input id="link-text" placeholder="Link Text">
                            </div>
                        </div>

                    </div>


                    <form id="wp-resource-link">


                        <div id="resource-link-selector">
                            <div id="resource-search-panel">
                                <h2 id="resource-link-modal-title">Choose Resource</h2>
                                <div id="resource-most-recent-results" class="query-results" tabindex="0">
                                    <ul>
                                        <?php $args = array(

                                     'posts_per_page' => -1,
                                     'orderby' => 'title' ,
                                     'order'   => 'ASC',
                                     'post_type' => 'resources');

                                 $loop = new WP_Query( $args );
                                 while ( $loop->have_posts() ) : $loop->the_post(); ?>


                                <li class="alternate">
                                    <input type="hidden" class="item-permalink" value="<?php the_ID();?>"><span class="item-title"><?php echo get_the_title();?></span><span class="item-info"><?php echo get_the_ID();?></span></li>

                                            <?php endwhile;?>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="submitbox">
                            <div id="wp-resource-link-update">
                                <input type="submit" value="Insert Resource Link/Button" class="button button-primary insert-link" id="" name="">
                            </div>
                        </div>
                    </form>
                </div>

                 <div id="insert-video" style="display: none;">



                    <h1 id="link-modal-title">Insert YouTube Video</h1>
                    <div class="acf-field acf-field-oembed" data-name="" data-type="oembed" data-key="" data-width="100">

                        <div class="acf-input">
                            <div class="acf-oembed has-value" data-width="640" data-height="390">

                                <div class="title acf-soh">

                                    <div class="title-value">

                                        <form id="alignment" action="">
                                            <p>Video Alignment</p>
                                            <label>Left
                                                <input type="radio" name="type" value="alignleft" class="alignment">
                                            </label>
                                            <label>Center
                                                <input type="radio" name="type" value="aligncenter" class="alignment">
                                            </label>
                                            <label>Right
                                                <input type="radio" name="type" value="alignright" class="alignment">
                                            </label>
                                            <p>Video URL</p>
                                            <input id="video-url" placeholder="Insert YouTube URL">

                                            <div class="caption">
                                                <p>Video Caption (Optional)</p>
                                            <input id="caption" placeholder="Add Video Caption - Optional">
                                            </div>

                                        </form>
                                    </div>

                                </div>


                                <div class="canvas">

                                    <div class="canvas-loading">
                                        <i class="acf-loading"></i>
                                    </div>

                                    <div class="canvas-media" data-name="value-embed">
                                        <iframe id="ext-video-preview" src="" width="640" height="360" frameborder="0" title="" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                                    </div>

                                    <i class="acf-icon -picture hide-if-value"></i>

                                </div>
                                <br>
                                <div class="submitbox">
                                    <div id="wp-link-update">
                                        <input type="submit" value="Insert Video" class="button button-primary ext-insert-video" id="" name="">




                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                </div>
<?php }

//Resource Button TinyMCE


function enqueue_plugin_scripts($plugin_array)
{
    //enqueue TinyMCE plugin script with its ID.
    $plugin_array["link_button"] =  get_template_directory_uri() . "/admin/js/button.js";
    return $plugin_array;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts");

function register_buttons_editor($buttons)
{
    //register buttons with their id.
    array_push($buttons, "resource-link, insert-video, insert-columns, insert-hr");
    return $buttons;
}

add_filter("mce_buttons", "register_buttons_editor");


//Tags for Pages

// add tag support to pages
function tags_support_all() {
    register_taxonomy_for_object_type('post_tag', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
    if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

if ( ! defined( 'WPINC' ) ) {
    die;
}

// Only run plugin in the admin
if ( ! is_admin() ) {
    return false;
}

// Disable Spam filter for Gravity Forms
function flun_disable_spam()
{
  return false;
}
add_filter("gform_entry_is_spam", "flun_disable_spam");





