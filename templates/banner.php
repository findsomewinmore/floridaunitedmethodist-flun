<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>

        <div class="general-banner">
            <div class="row">

                <img class="banner" src="<?php echo $banner['sizes']['banner']; ?>" />
                <div class="col-md-5 offset-md-4 banner-text text-center">
                    <h1 role="heading"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
