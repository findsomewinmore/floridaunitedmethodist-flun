<?php get_template_part('templates/head'); ?>
<?php
$primary_branding = get_field('primary_branding', 'option');
$social_services = get_field('social_services', 'option');

?>
<header id="header">
<a href="/"><img class="primary-branding text-center" src="<?php echo $primary_branding['url'] ?>" /></a>
  <button class="toggle-menu c-hamburger">
    <span class="toggle-menu__btn"></span>
      </button>
    <span class="menu-toggle">Menu</span>
    <a href="<?php the_permalink(3739);?>" class="header-btn account"><i class="fi flaticon-user"></i> Account Access</a>
    <a href="<?php the_field('file', 4460);?>" class="header-btn downloadable"><i class="fa fa-line-chart"></i> Fund Info</a>
    <a href="/?s" class="header-btn"><i class="fa fa-search"></i> Search</a>
    <a href="/contact-us" class="header-btn"><i class="fa fa-comment-o"></i> Contact Us</a>
    <a href="/category/news/" class="header-btn"><i class="fa fa-newspaper-o"></i> News</a>
    <!--<div class="header-btn english"><?php //echo do_shortcode('[glt language="English" label="English" image="no" text="yes"]');?></div>
    <div class="header-btn spanish"><?php //echo do_shortcode('[glt language="Spanish" label="Español" image="no" text="yes"]');?></div>-->
    <a class="header-btn language" href="#"><i class="fa fa-globe"></i>Language<?php echo do_shortcode('[google-translator]'); ?></a>

      <div class="nav-social">
         <ul>

         <?php foreach($social_services as $key => $social_service){?>
          <?php
              $service = $social_service['service'];
              $service_url = $social_service['service_url'];
          ?>

         <li class="social-header"> <a class="social-icons" href="<?php echo $service_url ?>"> <i class="fa <?php echo $service ?>"></i></a></li>

        <?php } ?>
      <!--     <li class="social-header">
              <i class="fa fa-facebook-official" aria-hidden="true"></i>
            </li>
            <li class="social-header">
               <i class="fa fa-twitter" aria-hidden="true"></i>
            </li>
            <li class="social-header">
              <i class="fa fa-youtube-play" aria-hidden="true"></i>
            </li> -->
           </ul>
      </div>
</header>

<div class="nav-drawer">
    <div class="bg"></div>
    <nav role="navigation">
        <div class="container">
            <div class="row nav-row">
                <div class="col-xl-3 col-sm-3">
                <div id="nav-search">
                    <div class="search-container">

                        <?php echo get_search_form();?>
                    </div>


                </div>
                </div>
                <div class="col-xl-3 col-sm-3">
                    <a href="<?php the_permalink(3739);?>" class="account"><i class="fi flaticon-user"></i> Account Access</a>

                </div>
                <div class="col-xl-3 col-sm-3">
                    <a href="<?php the_field('file', 4460);?>" class="downloadable"><i class="fa fa-line-chart"></i> Fund Performance</a>
                </div>
                <div class="col-xl-3 col-sm-3">
                    <a href="/contact-us"><i class="fa fa-comment-o"></i> Contact Us</a>
                </div>
            </div>
            <div class="primary-menu">
                <?php wp_nav_menu( array( 'theme_location' => 'primary-menu') ); wp_reset_postdata();?>
            </div>
            <!--
      <div class="col-md-12 primary-menu-2">
        <?php //wp_nav_menu( array( 'theme_location' => 'primary-menu-2', 'container_class' => 'row') ); ?>
      </div>-->

        </div>

    </nav>

</div>
