<?php
/**
 * The template for displaying the header
 *
 * @package Smores
 * @since Smores 2.0
 */
?>
<!doctype html>

<html class="no-js" <?php language_attributes(); ?> id="returnTop">


<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta name="description" content="<?php bloginfo('description') ?>" />
  <meta name="author" content="Findsome &amp; Winmore" />
  <!-- Google will often use this as its description of your page/site. Make it good. -->

  <meta name="google-site-verification" content="" />
  <!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->

  <meta name="Copyright" content="<?php echo date('Y'); ?>" />

  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-touch-fullscreen" content="yes" />

   <!-- FontAwesome -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Gentium+Basic" rel="stylesheet">

  <!-- Slick.js -->
  <?php get_template_part('partials/meta', 'favicons'); ?>

  <?php
    /* Don't List Unlisted Pages */
    if (get_field('exclude_from_az_index')) {
      echo '<meta name="robots" content="noindex,nofollow">';
    }

    /* Wordpress Head */
    wp_head();
  ?>
</head>

<body <?php body_class() ?>>



<div id="newsletter-modal" class="modal">
    <div class="container">

            <div class="row">

                <div class="col-lg-8 offset-lg-2">
                     <div class="inner">
                         <a href="#" class="close"></a>

                        <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
                </div>
            </div>

        </div>
    </div>
</div>
    </div>
<div class="loader"></div>
<?php get_template_part('partials/notice', 'outdated'); ?>
