<?php
/**
 * The template for displaying the footer
 *
 * @package Smores
 * @since Smores 2.0
 */
?>
    <footer class="flex-rw">

    <?php

      $primary_branding = get_field('primary_branding', 'option');
      $footer_physical_address = get_field('footer_physical_address', 'option');
      $footer_mailing_address = get_field('footer_mailing_address', 'option');
      $secondary_branding = get_field('secondary_branding', 'option');
      $footer_branding = get_field('footer_branding', 'option');
      $social_services = get_field('social_services', 'option');
      $footer_site_terms = get_field('footer_site_terms', 'option');
      $footer_copyright = get_field('footer_copyright', 'option');
      $primary_phone_number = get_field('primary_phone_number', 'option');

    ?>



 <div class="container">
     <div class="col-xl-12 offset-xl-0 col-lg-12 offset-lg-0 col-md-12">
<div class="row">
    <div class="col-md-5">
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_id' => 'footer-menu', 'after' => '' ) ); ?>
    </div>

    <div class="col-md-6 offset-md-1">


        <?php if( !empty($footer_branding)):?>
			<div class="row address">
				<div class="col-md-5">
					<p class="follow-title">FOLLOW US</p>
        <div class="footer-social-overlap footer-social-icons-wrapper">

        <?php foreach($social_services as $key => $social_service){?>
          <?php
              $service = $social_service['service'];
              $service_url = $social_service['service_url'];

          ?>

          <a class="social-icons" href="<?php echo $service_url ?>"> <i class="fa <?php echo $service ?>"></i></a>
        <?php } ?>


     </div>
					<img class="footer-branding" src="<?php echo $footer_branding['url']; ?>" />
				</div>
				<div class="col-md-7 text-center text-md-left mb-2">
					<style>
						#cefex-logo {transition: 0.2s; }
						#cefex-logo:hover { opacity: 0.8; color: #4591a2; }
					</style>
					<a href="https://www.fumf.org/church-agency-investments/cefex-certification/" rel="noopener nofollow" target="_blank" id="cefex-logo">
						<img class="footer-branding" src="https://www.fumf.org/wp-content/uploads/CEFEX-1.png" style="width: auto; margin-bottom: 0.5em" alt="CEFEX Certified" />
					</a>
						<a href="https://www.fumf.org/wp-content/uploads/2020-CEFEX-Certificate.pdf" rel="noopener nofollow" target="_blank" alt="Investment Steward Certified">
							<b style="display: inline-block; font-size: 0.8rem;">Investment Steward Certified</b>
						</a>
					
					<a class="btn" style="font-size: 14px; padding: 0.75rem 1.5rem; margin-top:10px;" href="https://www.surveymonkey.com/r/howdidyou" target="_blank" rel="noopener noreferrer">connections</a><br>
							<a class="btn modal-toggle" style="font-size: 14px; padding: 0.75rem 1.5rem; margin-top:10px; min-width:150px;" href="#newsletter-modal" target="_blank" rel="noopener noreferrer">SUBSCRIBE</a>
					
				</div>
			</div>
            <?php endif ?>
                <div class="row address">
                    <div class="col-md-5">
						
						
                        <p class="footer-text">
                            <?php echo $footer_physical_address ?>
                        </p>
                    </div>
                    <div class="col-md-7">
                        <p class="footer-text">
                            <?php echo $footer_mailing_address ?>
                        </p>
						<div>
							
					</div>
                    </div>
                </div>
    </div>


</div>
<div class="row mt-1">
    <div class="col-md-6">
        <?php echo $footer_site_terms ?>
    </div>
    <div class="col-md-6">

                    <?php foreach($primary_phone_number as $key => $primary_number){ ?>
                        <?php
                                $phone_label = $primary_number['phone_label'];
                                $phone_number = $primary_number['phone_number'];
                              ?>
                            <a class="footer-text" href="tel:<?php echo $phone_number ?>">
                                <?php if($phone_label == 'Fax'):?>
                                <i class="fa fa-fax"></i>
                                <?php else:?>
                                <i class="fa fa-phone"></i>
                                <?php endif;?>
                                <?php echo $phone_label ?>:
                                    <?php echo $phone_number ?>
                            </a>
                            <?php } ?>

    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6">
        <p class="text-white serif">&copy; <?php echo date("Y"); ?> <?php echo $footer_copyright;?></p>
    </div>
    <div class="col-md-6">
                         <a href="https://findsomewinmore.com" target="_blank"><img class="fiwi-footer" src="<?php echo get_template_directory_uri(); ?>/dist/img/FIWI-classic-website-by-white.svg"></a>
    </div>

     </div>
</div>

</footer>
      </div>

    <?php wp_footer(); ?>





     <script>
    $(window).load(function() {
        outdatedBrowser({
            bgColor: '',
            color: '#ffffff',
            lowerThan: 'transform',
            languagePath: ''
        })

    });

    function get_browser() {
        var ua = navigator.userAgent,
            tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return {
                name: 'IE',
                version: (tem[1] || '')
            };
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR|Edge\/(\d+)/)
            if (tem != null) {
                return {
                    name: 'Opera',
                    version: tem[1]
                };
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
        }
        return {
            name: M[0],
            version: M[1]
        };
    }


    var browser = get_browser();
    $('span.version').text(browser.version);
      </script>

</body>
</html>
