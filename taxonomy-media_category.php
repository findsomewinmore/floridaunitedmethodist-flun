<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>




<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" />
        <div class="overlay"></div>
            <div class="container">

                <h1 class="single-info"><?php single_cat_title(); ?></h1>
            </div>

        </section>
    <section class="pb-0">

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">

                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => 39 );
                    wp_nav_menu( $menu_args );
                    ?>

            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0">
              <blockquote><?php
                echo term_description();
              ?>
            </blockquote>
            </div>
        </div>
    </div>


    </section>
    <section>
        <div class="container">


            <div class="row">
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>

                            <?php $image = get_the_ID();
                            $type = get_post_mime_type($image); ?>



                            <?php if($type == 'image/jpeg'):?>

                                <a href="<?php echo wp_get_attachment_url($image, 'medium'); ?>" class="col-sm-4 gallery-item image">
                                    <?php echo wp_get_attachment_image($image, 'large'); ?>
                                    <p><?php echo get_the_excerpt($image);?></p>
                                </a>

                            <?php endif;?>

                <?php if($type == 'application/pdf'):


                $filename_only = basename( get_attached_file( $image ) );
                $name = preg_replace('/[_-]+/', ' ', trim($filename_only));
                $name1 = str_replace(".pdf","", $name);?>


                                <div class="col-sm-12">
                                    <a href="<?php echo wp_get_attachment_url($image); ?>"><?php echo get_the_title($report['ID']);?>

                                </a>
                                </div>

                <?php endif;?>

                 <?php endwhile; endif;?>

            </div>

        </div>

    </section>


    <section class="pagination">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="nav-next alignright"><?php previous_posts_link( 'Previous' ); ?></div>

                </div>
                <div class="col-sm-6">

                <div class="nav-previous alignleft"><?php next_posts_link( 'More' ); ?></div>

                </div>

            </div>

        </div>


    </section>

</div>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>
