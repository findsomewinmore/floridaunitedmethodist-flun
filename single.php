<?php
/**
 * The Template for displaying all single posts
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
?>
<!-- Banner -->

<div class="main-container">
        <section class="page-banner">

            <img <?php if(get_field('downloadable')): echo 'class="downloadable"'; endif;?> src="<?php if($banner !== null): echo $banner['sizes']['banner']; else: echo '' . get_template_directory_uri(). '/dist/img/flun-18.jpg'; endif;?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
                    <div class="overlay"></div>

            <div class="container">


                <div class="row">
                    <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

                        <h3 class="text-center text-white"><?php echo get_the_date('F d, Y'); ?></h3>
                        <h1 role="heading"><?php if(get_field('headline')): echo get_field('headline'); else: echo get_the_title(); endif;?></h1>
                        <hr class="historyhr">
                    <div class="author-meta">

                        <div class="row">

                            <div class="col-sm-8 offset-sm-2">
                                <div class="row">


                                <?php

                                $authorType = get_field('author_type');

                                if($authorType == 'staff'):

                                ?>
                                <?php $authors = get_field('author');

                                foreach($authors as $author):?>
                                <div class="col-md">
                                    <div class="author-mast">
                                    <img src="<?php echo get_template_directory_uri(); ?>/dist/img/clear.gif" style="background-image: url(<?php echo get_field('bio_image', $author->ID)['sizes']['medium'];?>); background-size: cover; background-position: 50% 25%;">
                                    <h3 class="text-center text-white">By <?php echo get_the_title($author->ID);?></h3>
                                    </div>


                                </div>

                                    <?php endforeach;?>

                                <?php elseif($authorType == 'guest'):?>



                                <?php if( have_rows('guest_authors') ):?>
                                <?php while ( have_rows('guest_authors') ) : the_row(); if(get_sub_field('author_image')):?>




                                    <div class="col-md">
                                    <div class="author-mast">

                                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/clear.gif" style="background-image: url(<?php echo get_sub_field('author_image')['sizes']['medium'];?>); background-size: cover; background-position: 50% 25%;">
                                        <?php else:?>
                                <div class="col">
                                    <div class="author-mast">


                                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/clear.gif" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/img/guest.svg); background-size: 80%; background-repeat: no-repeat; background-color: white; background-position: 50% 50%;">

                                            <?php endif;?>
                                                <h3 class="text-center text-white">By <?php the_sub_field('author_name');?></h3>

                                    </div>
                                </div>


                                <?php endwhile; ?>
                                <?php endif; ?>

                                <?php else:?>

                                    <div class="col-md">
                                        <div class="author-mast">

                                            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/clear.gif" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/img/logo.svg); background-size: 80%; background-repeat: no-repeat; background-color: white; background-position: 50% 50%;">
                                            <h3 class="text-center text-white">By FUMF</h3>
                                        </div>
                                    </div>
                                <?php endif;?>
                                </div>


                            </div>

                        </div>

                    </div>

                    </div>
                </div>
            </div>

            <p class="banner-caption"><?php echo $banner['caption'];?></p>

        </section>




<section class="">

    <div class="container">

    <div class="row">

        <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">

            <div class="post-body">

            <div class="date-archive">
                <div class="row">
                    <div class="col-sm-6">
                      <?php echo 'Select a month'; ?>
                <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                    <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
                </select>
                    </div>
                    <div class="col-sm-6">
                      <?php echo 'Select a category'; ?>
                <?php wp_dropdown_categories( 'hide_empty=0&exclude=6,7,8,13,57' ); ?>
                <script type="text/javascript">

                    var dropdown = document.getElementById("cat");
                    function onCatChange() {
                        if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                        }
                    }
                    dropdown.onchange = onCatChange;

                </script>


                    </div>

                </div>

            </div>

            <?php
                if (have_posts()) :
                   while (have_posts()) :
                      the_post();
                         the_content();?>


                            <?php if(get_field('author_excerpt')): echo '<div class="author-meta">' . get_field('author_excerpt') . '</div>'; endif;?>




                   <?php endwhile;

                   comment_form();

                  echo '<ol class="commentlist">';
                    $comments = get_comments(array('post_id'=>get_the_ID(), 'status' => 'approve'));
                    wp_list_comments(array('echo'=>true), $comments);
                  echo '</ol>';
                endif;
            ;?>
             <?php the_field('script');?>
            </div>



            </div>
    </div>
    </div>




    </section>

<section>
        <div class="container">
            <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">
                <?php
                  $cats = get_the_category();
                  $slug = $cats[0]->slug;

                  if( $slug == 'news' || $slug == 'blog' )
                  {
                    if( $cats[1] )
                    {
                      $slug = $cats[1]->slug;

                      if( $slug == 'news' || $slug == 'blog' )
                      {
                        if( $cats[2] )
                        {
                          $slug = $cats[2]->slug;
                        }
                      }
                    }
                  }
                ?>

                <a href="/category/<?php echo $slug; ?>" class="btn">See Related News</a>
                <div></div>
        </div>
        </div>

</section>
<div class="share-bar">
<p>Share</p>
<ul>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>"><i class="fa fa-facebook"></i></a></li>
    <li><a href="https://twitter.com/home?status=<?php the_permalink();?>"><i class="fa fa-twitter"></i></a></li>
    <li><a href="https://plus.google.com/share?url=<?php the_permalink();?>"><i class="fa fa-google-plus"></i></a></li>
    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>"><i class="fa fa-linkedin"></i></a></li>
    <li><a href="mailto:?subject=<?php echo get_the_title();?>&body=Check%20out%20this%20link%20<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/envelope.svg"></a>
    <li class="hidden-sm-down"><a href="#" class="print-toggle"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/print.svg"></a></li>
</ul>

</div>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>
    </div>
