<?php
/**
 * Template Name: Contact
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>
<!-- Banner -->
    <?php

      $primary_branding = get_field('primary_branding', 'options');
      $footer_physical_address = get_field('footer_physical_address', 'options');
      $footer_mailing_address = get_field('footer_mailing_address', 'options');
      $secondary_branding = get_field('secondary_branding', 'options');
      $footer_branding = get_field('footer_branding', 'options');
      $social_services = get_field('social_services', 'options');
      $footer_site_terms = get_field('footer_site_terms', 'options');
      $footer_copyright = get_field('footer_copyright', 'options');
      $primary_phone_number = get_field('primary_phone_number', 'options');

    ?>


<div class="main-container">
    <section class="page-banner">

        <div class="map-container">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14084.897171348937!2d-81.9620368!3d28.0481754!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd239bcbc94242449!2sThe+Florida+United+Methodist+Foundation!5e0!3m2!1sen!2sus!4v1500494653684" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
            <div class="container">
            </div>

        </section>
    <section class="">
       <div class="container">
        <div class="row">


            <div class="col-md-5 post-body">

            <h1 role="heading"><?php echo get_the_title();?></h1>

            <?php
                if (have_posts()) :
                   while (have_posts()) :
                      the_post();
                         the_content();
                   endwhile;
                endif;
            ;?>

                <address>
                    <div class="">
                    <?php the_field('hours', 'options')?>

                    </div>

                    <h5>Local Office</h5>
                    <p><?php the_field('footer_physical_address', 'options')?></p>
                    <h5>Mailing Address</h5>
                    <p><?php the_field('footer_mailing_address', 'options')?></p>
                    <a class="btn mb-2" href="mailto:<?php the_field('email', 'options')?>"><i class="fa fa-envelope"></i> <?php the_field('email', 'options')?></a>
                    <?php foreach($primary_phone_number as $key => $primary_number){ ?>
                        <?php
                                $phone_label = $primary_number['phone_label'];
                                $phone_number = $primary_number['phone_number'];
                              ?>
                            <a class="btn mb-2" href="tel:<?php echo $phone_number ?>">
                                <?php if($phone_label == 'Fax'):?>
                                <i class="fa fa-fax"></i>
                                <?php else:?>
                                <i class="fa fa-phone"></i>
                                <?php endif;?>
                                <?php echo $phone_label ?>:
                                    <?php echo $phone_number ?>
                            </a>
                            <?php } ?>


                </address>

            </div>

            <div class="col-md-6 offset-md-1 post-body">


                    <?php the_field('form');?>

            </div>


        </div>
    </div>

    </section>


<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>


</div>
