<?php
/*
 * Template Name: Service Stories
 * Description: Our Stories section Loans
 */
?>

<?php get_template_part('templates/header'); ?>
        <?php
            $banner = get_field('banner');
        ?>
<div class="main-container">

<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>

    <section class="pb-0">

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">
            <?php if ( $post->post_parent ) { ?>
                <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>
                <?php } ?>

                <?php echo do_shortcode('[fumf-childpages]');?>
            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 about-quote">
                <div class="get-quote"></div>
            </div>
        </div>
    </div>


    </section>
<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>


</div>
