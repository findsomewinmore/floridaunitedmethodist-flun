<?php
/*
 * Template Name: Board Members
 * Description: A template for members of the board
 */
?>
<?php get_template_part('templates/header'); ?>

    <?php
        $banner = get_field('banner');

    ?>

<div class="main-container">
    <?php include(locate_template('partials/parts/banner.php', false, false)); ?>

<section>

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">
                    <!--<a href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a>-->
                    <?php //echo do_shortcode('[fumf-childpages]');
                    ?>

                    <?php $menu = get_field('page_menu'); ?>

                    <?php
                    $menu_args = array('menu' => $menu);
                    wp_nav_menu($menu_args);
                    ?>

            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 about-quote">
                        <blockquote>The foundation is governed by a board of directors elected annually by the foundation’s membership at the annual meeting of the Florida Conference. The board, which meets quarterly, includes clergy and laity from across the conference who have expertise in a variety of professions. Members are elected to three-year terms and may serve a maximum of nine consecutive years.</blockquote>
            </div>
        </div>
    </div>


    </section>
    <section>


    <div class="container">
    <div class="">
        <h3 class="mt-2">Officers and Committee Leaders</h3>
        </div>

        <div class="row">

                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                    'orderby' => 'menu_order title',
                    'order'   => 'ASC',
                                        'post_type' => 'team_members',
//                                        'meta_key' => 'staff_type',
//                                        'meta_value' => 'board',
                                        'meta_query' => array(
                                            'relation' => 'AND',

                                            'btitle' => array(
                                                'key' => 'title',
                                                'value' => '',
                                                'compare' => '!='
                                            ),
                                            'type' => array(
                                                'key' => 'staff_type',
                                                'value' => 'board',
                                                'compare' => 'LIKE'
                                            ),

                                        ),
//                                            'orderby' => array(
//
//                                                'btitle' => 'ASC',
//                        //						'title' => 'DESC',
//
//                                            ),
                                        );

                add_filter('posts_orderby', 'posts_orderby_lastname');

                $loop = new WP_Query($args);

                $officer = get_field('officer_list', 'option') ? get_field('officer_list', 'option') : []; 

                $loop = count($officer) && count($officer) > 0 ? $officer : $loop; 

                // if ($loop->have_posts())
                //  while ($loop->have_posts()) : $loop->the_post(); 
                foreach($loop as $post):

                    if($officer) :
                        $post = $post['officer_post'];
                 
                    endif;
                   
                    $officer_id = $post->ID;
                 ?>

    <?php

                        $board_image = get_field('board_image', $officer_id);
                        $information = get_field('information', $officer_id);
    ?>

                    <div class="col-xl-3 col-lg-4 col-md-4">
                            <div class="board-individual">
                            <a style="width: 100%; height: 100%; position: absolute; top:0; left:0; z-index: 1; opacity: 0" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
                            <div class="row">
                                <div class="col-md-12">
                                        <img class="bio-image greyscale" src="<?php echo $board_image['sizes']['large']; ?>" alt="<?php echo get_the_title() ?> - Board Member - <?php bloginfo('name'); ?>" />
                                    <div class="board-title-container">
                                    <h4 class="name"><?php echo get_the_title() ?></h4>
                                            <?php if (get_field('title', $officer_id)) : echo '<h5 class="board-title">' . get_field('title', $officer_id) . '</h5>';
                                            endif; ?>
                                    </div>


                                </div>
<!--                                <div class="col-md-7">-->
                                    <div class="bio-wrapper">
                                        <h4 class="name" style="color: #fff; font-size: 1rem; margin: 1rem 0;"><?php echo get_the_title() ?></h4>
                                        <?php if (get_field('title', $officer_id)) : echo '<h5 class="board-title">' . get_field('title', $officer_id) . '</h5>';
                                        endif; ?>
                                            <span class="bio-blurb"><?php echo $information ?></span>
                                        <div style="color: #fff; font-size: 1rem; text-decoration: underline; position: absolute; bottom: 1.5rem; left: 50%; transform: translate(-50%, -50%)"><span>Read Bio</span></div>
                                    </div>
<!--                                </div>-->
                            </div>

                        </div>
                    </div>

                <?php endforeach;
                // endif;
                wp_reset_postdata(); ?>


        </div>

    <div class="row">
        <div class="col-md-12">
        <h3>Board Members</h3>
        </div>
        </div>
    <div class="row">

                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                    'orderby' => 'menu_order title',
                    'order'   => 'ASC',
                                        'post_type' => 'team_members',
                                        'meta_query' => array(
                                            'relation' => 'AND',
                                            'btitle' => array(
                                                'key' => 'title',
                                                'value' => '',
                                                'compare' => '='
                                            ),
                                            'type' => array(
                                                'key' => 'staff_type',
                                                'value' => 'board',
                                                'compare' => 'LIKE'
                                            ),


                                        ),

                                        );


                add_filter('posts_orderby', 'posts_orderby_lastname');

                $loop = new WP_Query($args);

                $board = get_field('board_list', 'option') ? get_field('board_list', 'option') : []; 

                $loop = count($board) && count($board) > 0 ? $board : $loop; 

                // if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); 
                foreach($loop as $post):

                    if($board) :
                        $post = $post['board_post'];
                 
                    endif;
                   
                    $board_id = $post->ID;
                
                ?>

    <?php

                        $board_image = get_field('board_image', $board_id);
                        $information = get_field('information', $board_id);
    ?>

                    <div class="col-xl-3 col-lg-4 col-md-4">
                            <div class="board-individual">
                                <a style="width: 100%; height: 100%; position: absolute; top:0; left:0; z-index: 1; opacity: 0" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
                            <div class="row">
                                <div class="col-md-12">
                                        <img class="bio-image greyscale" src="<?php echo $board_image['sizes']['large']; ?>" alt="<?php echo get_the_title() ?> - Board Member - <?php bloginfo('name'); ?>" />
                                    <div class="board-title-container">
                                    <h4 class="name"><?php echo get_the_title() ?></h4>
                                            <?php if (get_field('title', $board_id)) : echo '<h5 class="board-title">' . get_field('title', $board_id) . '</h5>';
                                            endif; ?>
                                    </div>
                                </div>
<!--                                <div class="col-md-7">-->
                                    <div class="bio-wrapper">
                                        <h4 class="name" style="color: #fff; font-size: 1rem; margin: 1rem 0;"><?php echo get_the_title() ?></h4>
                                        <?php if (get_field('title', $board_id)) : echo '<h5 class="board-title">' . get_field('title', $board_id) . '</h5>';
                                        endif; ?>
                                            <span class="bio-blurb"><?php echo $information ?></span>
                                        <div style="color: #fff; font-size: 1rem; text-decoration: underline; position: absolute; bottom: 1.5rem; left: 50%; transform: translate(-50%, -50%)"><span>Read Bio</span></div>
                                    </div>
<!--                                </div>-->
                            </div>

                        </div>
                    </div>

                <?php endforeach;
                
                wp_reset_postdata(); ?>

    </div>
    </div>

    </section>

    <?php include(locate_template('partials/parts/cta.php', false, false)); ?>
<?php get_template_part('templates/footer'); ?>
</div>
