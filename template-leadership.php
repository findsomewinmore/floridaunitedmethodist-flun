<?php
/*
 * Template Name: Leadership
 * Description: Leadership Page
 */
?>

<?php get_template_part('templates/header'); ?>


        <?php
            $leader_banner = get_field('leader_banner');
            $banner = get_field('banner');
            $navigation = get_field('navigation');
            $navigation_blurb = get_field('navigation_blurb');
            $leader_gallery = get_field('leader_gallery');
            $cta_title = get_field('cta_title');
            $cta = get_field('cta');
        ?>

<!-- Banner -->

<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>


<!-- Navigation -->

    <div class="leader-nav-section mb-1">
        <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 leader-nav about-nav">
                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => $menu );
                    wp_nav_menu( $menu_args );
                    ?>
            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 leader-blurb">
                <?php echo $navigation_blurb ?>
            </div>
        </div>

        </div>

    </div>


<!-- Leadership Section -->
    <section class="leadership-loop">
<div class="mb-2">
<div class="container">



    <div class="leader-bio-container single-leader">


            <?php 
	
				/* $args = array(

                'posts_per_page' => -1,
                'order'   => 'ASC',
                'orderby' => 'menu_order btitle',
                'post_type' => 'team_members',
                                        'meta_query' => array(
                                            'relation' => 'AND',

                                            'btitle' => array(
                                                'key' => 'order',
                                                'compare' => 'EXISTS'
                                            ),
                                            'type' => array(
                                                'key' => 'staff_type',
                                                'value' => 'staff',
                                                'compare' => 'LIKE'
                                            ),


                                        ),
); */
	            $args = array(

                'posts_per_page' => -1,
               'order'   => 'ASC',
               'orderby' => 'menu_order',
                'post_type' => 'team_members',
                'meta_query' => array(

                    'type' => array(
                        'key' => 'staff_type',
                        'value' => 'staff',
                        'compare' => 'LIKE'
                    ),
                                            ),

);
            add_filter( 'posts_orderby' , 'posts_orderby_lastname' );

            $loop = new WP_Query( $args );
            // $loop = new WP_Query( $args );

            $staff = get_field('staff_list', 'option') ? get_field('staff_list', 'option') : []; 

            $loop = count($staff) && count($staff) > 0 ? $staff : $loop; 


            foreach($loop as $post):
            //  ( $loop->have_posts() ) : $loop->the_post(); 
            if($staff) :
                $post = $post['staff_post'];
         
            endif;
           
            $staff_id = $post->ID;
            
            ?>

                <?php
                    $leader_image = get_field('bio_image', $staff_id);
                    $leader_name = get_the_title();

                    $leader_mini_bio = get_field('bio_text', $staff_id);
                    $button_url = get_field('button_url', $staff_id);
                    $button_text = get_field('button_text', $staff_id);
                    $designation = get_field('designation', $staff_id);
                ?>
                    <div class="bio-box" id="slide-<?php $var = sanitize_title_for_query( get_the_title() ); echo esc_attr( $var);?>">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="bio-image greyscale" src="<?php echo $leader_image['url'] ?>" alt="<?php echo get_the_title() ?> - Staff - <?php bloginfo( 'name' ); ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="bio-container">
                                    <h4 class="leader-name"><?php echo $leader_name ?><?php if($designation != null): echo ', ' . $designation; endif; ?></h4>


                                    <?php if( have_rows('leadership_header', $staff_id) ):?>
                                    <?php while ( have_rows('leadership_header', $staff_id) ) : the_row();

                                        $leader_title = get_sub_field('title');
                                        $leader_phone = get_sub_field('email');
                                        $leader_email = get_sub_field('phone');


                                    ?>
                                     <h5 class="leader-title"><?php echo $leader_title ?></h5>
                                    <a class="bio-email" href="mailto:<?php echo $leader_phone ?>"><?php echo $leader_phone ?></a>
                                        <a class="bio-email" href="tel:<?php echo $leader_email ?>">
                                            <?php echo $leader_email ?>
                                        </a>


                                    <?php endwhile; ?>
                                    <?php endif; ?>



                                        <span class="mini-bio"><?php echo $leader_mini_bio ?></span>
                                        <a href="<?php the_permalink();?>" class="btn leader-btn">Read More
                                        </a>
                                </div>
                            </div>
                        </div>
                    </div>


    <?php endforeach; wp_reset_postdata();?>


                </div>


    </div>
</div>


    <div class="container directory hidden-sm-down">
        <h3>Staff Directory</h3>
            <ul class="">
            <?php /* $args = array(

                'posts_per_page' => -1,
                'order'   => 'ASC',
                'orderby' => 'menu_order btitle',
                'post_type' => 'team_members',
                                        'meta_query' => array(
                                            'relation' => 'AND',

                                            'btitle' => array(
                                                'key' => 'order',
                                                'compare' => 'EXISTS'
                                            ),
                                            'type' => array(
                                                'key' => 'staff_type',
                                                'value' => 'staff',
                                                'compare' => 'LIKE'
                                            ),


                                        ),

); */
				$args = array(

                'posts_per_page' => -1,
               'order'   => 'ASC',
               'orderby' => 'menu_order',
                'post_type' => 'team_members',
                'meta_query' => array(

                    'type' => array(
                        'key' => 'staff_type',
                        'value' => 'staff',
                        'compare' => 'LIKE'
                    ),
                                            ),

);
            add_filter( 'posts_orderby' , 'posts_orderby_lastname' );

            $loop = new WP_Query( $args );
            $staff2 = get_field('staff_list', 'option') ? get_field('staff_list', 'option') : []; 

            $loop = count($staff2) && count($staff2) > 0 ? $staff2 : $loop; 

            foreach($loop as $post):
         
            if($staff2) :
                $post = $post['staff_post'];
         
            endif;
           
            $staff2_id = $post->ID;
            //while ( $loop->have_posts() ) : $loop->the_post();
            
            $designation = get_field('designation', $staff2_id); ?>

            <li><a href="#slide-<?php $var = sanitize_title_for_query( get_the_title() ); echo esc_attr( $var);?>"><?php echo get_the_title();?><?php if($designation != null): echo ', ' . $designation; endif; ?></a></li>
            <?php endforeach; wp_reset_postdata();?>
            </ul>


    </div>
    </section>
    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>


<?php get_template_part('templates/footer'); ?>
</div>
