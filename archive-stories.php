<?php
/**
 * Template Name: Stories
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>




<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" />
            <div class="container">

                <h1 class="single-info">Our Stories</h1>
            </div>

        </section>


<?php $args = array(

    'posts_per_page' => -1,
    'orderby' => 'date' ,
    'order'   => 'DESC',
    'post_type' => 'ministry',
    'post_status' => 'publish'


);

$loop = new WP_Query( $args );


        if ($loop->have_posts()):
    while ($loop->have_posts()) : $loop->the_post(); ?>

            <?php
                $date = get_the_date();
                $title = get_the_title();
                $news_blurb = get_the_excerpt();
                $post_url = get_the_permalink();
                if(get_field('preview_image')): $banner = get_field('preview_image'); else: $banner = get_field('banner'); endif;
                if (has_category('unlisted'):
            ?>

                 <div class="news" style="background-image: url(<?php echo $banner['sizes']['banner']; ?>);">

                    <div class="container">

                        <div class="news-container col-md-10">
                            <h1 class="news_title"><?php echo $title ?></h1>
                            <p class = "news-blurb"><?php echo $news_blurb ?></p>
                            <a class="news-btn btn" href="<?php echo $post_url ?>">Read More</a>
                        </div>
                    </div>
                </div>

        <?php endif; endwhile; endif;?>





<?php get_template_part('templates/footer'); ?>
</div>
