(function () {



    tinymce.create("tinymce.plugins.link_button", {

        //url argument holds the absolute url of our plugin directory
        init: function (ed) {


            //Resource Link Button
            ed.addButton("resource-link", {
                title: "Add Resource Link",
                tooltip: 'Add Resource Link',
                icon: 'resource-link',
                onclick: function () {
                    //					text = prompt( "Enter Video Post ID", "" );
                    tb_show('', '#TB_inline?width=600&height=550&inlineId=resource-link');
                    return false;
                    //					window.location = '#TB_inline?width=600&height=550&inlineId=chs-video';
                    //					ed.execCommand( 'mceInsertContent', false, '[single-video id="' + text + '"]' );
                }
            });
            //Insert Video Button - to be aligned
            ed.addButton("insert-video", {
                title: "Insert YouTube Video",
                tooltip: 'Insert YouTube Video',
                icon: 'insert-video',
                onclick: function () {
                    //					text = prompt( "Enter Video Post ID", "" );
                    tb_show('', '#TB_inline?width=600&height=550&inlineId=insert-video');
                    return false;
                    //					window.location = '#TB_inline?width=600&height=550&inlineId=chs-video';
                    //					ed.execCommand( 'mceInsertContent', false, '[single-video id="' + text + '"]' );
                }
            });
               //Insert Columns Button
      ed.addButton("insert-columns", {
        title: "Insert Columns",
        tooltip: "Insert Columns",
        icon: 'insert-columns',
        onclick: function () {
          var medium = "6";
          var large = "6"
          ed.execCommand(
            "mceInsertContent",
            false,
            "[row][column medium=" +
              medium +
              " large=" +
              large +
              "][/column][column medium=" +
              medium +
              " large=" +
              large +
              "][/column][/row]"
          );
          return false;
        },
      });
      ed.addButton("insert-hr", {
        title: "Insert Green Hr",
        tooltip: "Insert Green Hr",
        icon: 'insert-green-hr',
        onclick: function () {
          ed.execCommand(
            "mceInsertContent",
            false,
            "[green-hr]"
          );
          return false;
        },
      });


            var linkType;
            var text;

            jQuery('.link-type').click(function () {


                linkType = jQuery(this).val();

            })




            //            jQuery('#link-text').bind('blur', function() {
            //
            //            var text = jQuery(this).val();
            //
            //            });



            jQuery('#wp-resource-link li.alternate').click(function (e) {

                e.preventDefault();
                jQuery('#wp-resource-link li.alternate').not(this).removeClass('selected');
                jQuery(this).addClass('selected');

            });

            jQuery('.insert-link').click(function (e) {
                //                e.preventDefault();
                var ID = jQuery('#resource-most-recent-results .selected .item-info').text();
                var text = jQuery('#link-text').val();

                if (linkType == 'resource-link') {

                    ed.execCommand('mceInsertContent', false, '[resource-link id="' + ID + '" button-label="' + text + '"]');

                } else {


                    ed.execCommand('mceInsertContent', false, '[resource-link-only id="' + ID + '" link-text="' + text + '"]');

                }

                tb_remove();
                //                jQuery('#link-text').val('');
                //                jQuery('#wp-resource-link li.alternate').removeClass('selected');
                return false;
            });



            var videoAlignment;

            jQuery('.alignment').click(function(){


                videoAlignment = jQuery(this).val();

            });


            jQuery('#video-url').bind('blur', function() {

            var video = jQuery(this).val();

            var	idOnly = video.split('https://www.youtube.com/watch?v=').pop();
            var noExtra = idOnly.replace('&feature=youtu.be','');

            jQuery('#ext-video-preview').attr('src', 'https://www.youtube.com/embed/' + noExtra);


            });


            jQuery('.ext-insert-video').click(function(){

                var ID = jQuery('#video-url').val();
                var	idOnly = ID.match(/v=([^&]+)/)[1];
                var caption = jQuery('#caption').val();


                ed.execCommand( 'mceInsertContent', false, '[YT-video alignment="' + videoAlignment + '" caption="' + caption + '" id="' + idOnly + '"]' );
                tb_remove();
                return false;
            });
            //			jQuery('#chs-video #wp-link-submit').click(function(){
            //
            //				var ID = jQuery('#chs-video .selected .item-info').text();
            //
            //				ed.execCommand( 'mceInsertContent', false, '[single-video id="' + ID + '"]' );
            //				tb_remove();
            //			});

            //			  window.send_to_editor = function (html) {
            //			  	imgurl = $('img', html).attr('src');
            //			  	image_field.val(imgurl);
            //			  	tb_remove();
            //			  }

        },

        createControl: function (n, cm) {
            return null;
        },

        getInfo: function () {
            return {

            };
        }
    });

    tinymce.PluginManager.add("link_button", tinymce.plugins.link_button);
})();
