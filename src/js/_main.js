/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {
 // Use this variable to set up the common and page specific functions. If you
 // rename this variable, you will also need to rename the namespace below.
 var Sage = {
  // All pages
  'common': {
   init: function () {
    // JavaScript to be fired on all pages

    //Form Logic for Downloadable Form

    if ($('.form-selector').length) {

     $('.form-selector select').change(function () {

      console.log($(this).val());

      $('.form-downloads a[title=" ' + $(this).val() + '"]').slideDown(0);

     });

    }

    // IE - Object Fix Polyfill

    if ($(".ie").length || $('.edge').length) {

     $('.page-banner > img, .history-center-image, .mission-row img, .values-image, .photo-slider__item img').each(function () {

      var image = $(this).attr('src');

      $(this).attr('style', 'background-image:url(' + image + ');  color:rgba(0,0,0,0); background-size: cover; background-position: 50% 25%;').attr('src', '/wp-content/themes/floridaunitedmethodist-flun/dist/img/clear.gif');

     })

     $('.grayscale').each(function () {

      function grayscale(src) {
       var canvas = document.createElement('canvas');
       var ctx = canvas.getContext('2d');
       var imgObj = new Image();
       imgObj.src = src;
       canvas.width = imgObj.width;
       canvas.height = imgObj.height;
       ctx.drawImage(imgObj, 0, 0);
       var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
       for (var y = 0; y < imgPixels.height; y++) {
        for (var x = 0; x < imgPixels.width; x++) {
         var i = (y * 4) * imgPixels.width + x * 4;
         var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
         imgPixels.data[i] = avg;
         imgPixels.data[i + 1] = avg;
         imgPixels.data[i + 2] = avg;
        }
       }
       ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
       return canvas.toDataURL();
      }

      $(this).attr('style', 'background-image:url(' + grayscale(this.src) + '); background-size: cover; background-position: 50%; color:rgba(0,0,0,0);').attr('src', '/wp-content/themes/floridaunitedmethodist-flun/dist/img/clear.gif');
     })

    }

    $('.modal-toggle, .modal-toggle a').click(function () {

     var link = $(this).attr('href');

     $(link).addClass('show');

     return false;

    })
    $('.inner .close').click(function () {

     $(this).parents('.modal').removeClass('show');

     return false;

    })

    var options = {
     valueNames: ['site-link']
    };

    //                var navSearch = new List('nav-search', options);

    var menuItem = '';

    $('.menu-item-has-children > a').not('.sub-menu .menu-item-has-children > a').each(function () {

     if ($(window).width() < 993) {

      var text = $(this).text();
      var link = $(this).attr('href');

      $(this).next('.sub-menu').prepend('<li><a href="' + link + '">' + text + '</a></li>');

      var menuItem = true;

     }

    });

    $(window).resize(function () {

     if ($(window).width() < 993) {

      if (menuItem = false) {

       $('.menu-item-has-children > a').not('.sub-menu .menu-item-has-children > a').each(function () {

        var text = $(this).text();
        var link = $(this).attr('href');

        $(this).next('.sub-menu').prepend('<li><a href="' + link + '">' + text + '</a></li>');

       });

      }

     }

    });

    $('.toggle-menu').on('click', function (e) {
     e.preventDefault();
     $('body').toggleClass('nav-drawer-in');
     $(this).toggleClass('is-active');

     //                    return false;
    });

    $('.main-container, .news_section, footer, .home-container').click(function () {

     $('body').removeClass('nav-drawer-in');
     $('.toggle-menu').removeClass('is-active');

    })

    $("#nav-search input").click(function () {

     $(this).addClass('search-active');
     $('body').addClass('search-open');

    });
    $(".search-clear").click(function (e) {

     $('#nav-search .search').removeClass('search-active').val('');
     $('body').removeClass('search-open');
     $('.nav-list').removeClass('show');

    });

    window.displayBoxIndex = -1;

    $("#nav-search input").keyup(function (e) {
     var NavigateSearch = function (diff) {
      displayBoxIndex += diff;
      var oBoxCollection = $(".searchwp-live-search-results > div");
      if (displayBoxIndex >= oBoxCollection.length)
       displayBoxIndex = 0;
      if (displayBoxIndex < 0)
       displayBoxIndex = oBoxCollection.length - 1;
      var cssClass = "active";
      oBoxCollection.removeClass(cssClass).eq(displayBoxIndex).addClass(cssClass);
     }
     $('.nav-list').addClass('show');

     if (e.keyCode == 40) {
      NavigateSearch(1);
     }
     if (e.keyCode == 38) {
      NavigateSearch(-1);
     }
     if (e.keyCode == 13) {

      var link = $("li.active a").attr('href');

      if (link == undefined) {

       return false;
      } else {

       window.location.href = link;

      }

     }

    });

    $('.home-video').click(function () {

     var link = $(this).attr('href');

     $(this).parents('.slides').find('iframe').attr('src', link);
     $(this).parents('.slides').find('.video-close').fadeIn(300);

     $('body').addClass('play');

     return false;

    });

    $('.social-icons, a[href^="https://ascaccess.net/AccuNet/FUMF"], a[href^="https://fumf.ficsloanstat.com"]').click(function () {

     var poplink = $(this).attr('href');
     newwindow = window.open(poplink, 'name', 'height=800,width=1200');
     if (window.focus) {
      newwindow.focus()
     }
     return false;

    });

    $('a').not('a[href="#"], .gallery-item.image').each(function () {

     $(this).attr('title', $(this).text());

    })
    $('.gallery-item.image').each(function () {

     var desc = $(this).find('p').text();

     $(this).attr('title', desc);

    })

    $('.language').click(function (e) {

     e.stopPropagation();

     $(this).toggleClass('toggled');
     return false;

    });
    $('body').click(function (e) {

     if ($('.language').hasClass('toggled')) {

      e.stopPropagation();
      $('.language').removeClass('toggled');
     }

    });

    if ($(window).width() > 993) {
     $('a').not('a[href="#"], a[href^="mailto:"], .downloadable, a[target="blank"], a[target="_blank"], .home-video, .modal-toggle, .social-icons, .account, input[type="submit"], a[href^="https://ascaccess.net/AccuNet/FUMF"], a[href^="https://fumf.ficsloanstat.com"], .results a, .faq-navigation a, .gallery-item.image, .directory a, a[href^="https://ascaccess.net/AccuNet/FUMF"], a[href^="https://fumf.ficsloanstat.com"], a[href^="http://www.fumfgift.org"], .modal-toggle a, .notranslate').click(function (e) {

      e.preventDefault();
      var link = $(this).attr('href');

      $('.loader').fadeIn(500);
      setTimeout(function () {
       window.location = link;
      }, 500);

      if ($('.mobile').length || $('.mac.safari').length) {

       $('.loader').delay(500).fadeOut(500);

      }

     })

    } else {

     $('a').not('a[href="#"], a[href^="mailto:"], .downloadable, a[target="blank"], a[target="_blank"], .home-video, .modal-toggle, .social-icons, .account, input[type="submit"], a[href^="https://ascaccess.net/AccuNet/FUMF"], a[href^="https://fumf.ficsloanstat.com"], .results a, .faq-navigation a, .gallery-item.image, .directory a, .menu-item-has-children > a, .modal-toggle a, .notranslate').click(function () {

      var link = $(this).attr('href');

      $('.loader').fadeIn(500);

      window.location = link;

      if ($('.mobile').length) {

       $('.loader').delay(500).fadeOut(500);

      }

     })

    }

    if ($(window).width() < 993) {

     $('.menu-item-has-children > a').not('.sub-menu li a').click(function () {

      $('.sub-menu').not($(this).next('.sub-menu')).not('.sub-menu li .sub-menu').slideUp(300);
      $(this).next('.sub-menu').not('.sub-menu li .sub-menu').slideToggle(300);

      return false;

     });
    }

    $('.video-close').click(function () {
     setTimeout(function () {
      $('iframe').attr('src', '');
     }, 500);

     $('body').removeClass('play');
     $(this).fadeOut(300);

    });

   },
   finalize: function () {
    // JavaScript to be fired on all pages, after page specific JS is fired
    //                window.resizeTo(screen.availWidth, screen.availHeight);

    if ($('.mac10').length) {

     $('.bio-box .bio-image').each(function () {

      var img = $(this).attr('src');

      $(this).parents('.col-md-4').attr('style', 'background-image:url(' + img + '); background-size: cover; filter: grayscale(1); background-position: 50%;');
      $(this).css('opacity', '0');

     });
    }

    var titleBar = window.outerHeight - window.innerHeight;

    //                            console.log(titleBar);

    if (titleBar < 38) {
     //Apply your logic here
     //                    console.log('Address bar not is visible');

     $('html').addClass('network-window');
    }
    $('p').each(function () {
     var $this = $(this);
     if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
      $this.remove();
    });

    if ($('.events-front').html() == false) {
     $('.events-container').remove();
     console.log('no events')

    }

    $('.directory a').click(function (e) {

     var slideIndex = $(this).closest('li').index();

     $(".single-leader").slick('slickGoTo', parseInt(slideIndex));

     e.preventDefault();
    });

    $('.gallery-item.image').magnificPopup({
     type: 'image',
     mainClass: 'mfp-fade',
     removalDelay: 300,
     gallery: {
      enabled: true
     }

    });

    $('.post-body p .btn').each(function () {

     $(this).parents('p').addClass('has-button');

    });

    //                $(document).on('click', '.downloadable', function () {
    //
    //                    //                        e.preventDefault();
    //
    ////                    var poplink = $(this).attr('href');
    ////                    newwindow = window.open(poplink, 'name', 'height=1800,width=1200');
    ////                    if (window.focus) {
    ////                        newwindow.focus()
    ////                    }
    ////                    return false;
    //
    //                });

    $('.downloadable').each(function () {

     $(this).attr('target', 'blank');

    })

    //                $(document).bind('gform_confirmation_loaded', function(){
    //                    $('.form-downloads').slideDown(500);
    //                });

    $('.post-body p iframe, .columns p iframe, .mission-row p iframe, .bio-text iframe, .bio-text p iframe').each(function () {

     $(this).wrap('<div class="iframe-wrapper"></div>');
     $(this).parents('p').addClass('mb0');

    });
    $('.mini-bio').each(function () {

     $(this).children('p').not(':first-child').remove();

    });

    $('.post-body').each(function () {

     var removeBreak = $(this).find('hr');

     $(removeBreak).next('p').remove();
     $(removeBreak).remove();

    });

    if (/iPhone/i.test(navigator.userAgent)) {

     $(window).scroll(function () {

      var windowfull = $.windowHeight();
      var windowHeight = $(window).height(),
       remaining = windowfull - windowHeight;

      console.log(windowfull);
      console.log(windowHeight);
      console.log(remaining);

      if (windowfull == windowHeight) {
       $('body').removeClass('share-up');

      } else {
       $('body').addClass('share-up');

      }

     });

    }

    if ($(window).width() > 768) {

     $('.post-body .alignright').each(function () {

      var width = $(this).outerWidth() / 2;

      $(this).css('margin-right', -width);

     })

     $('.post-body .alignleft').each(function () {

      var width = $(this).outerWidth() / 2;

      $(this).css('margin-left', -width);

     })

    }

    $('.post-body figure').click(function () {

     var img = $(this).find('img').attr('src').replace('-300x200', '').replace('-300x200', '').replace('-768x512', '').replace('-1024x683', '');

     $.magnificPopup.open({
      type: 'image',
      mainClass: 'mfp-fade',
      removalDelay: 300,
      items: {
       src: img
      },
      gallery: {
       enabled: true
      }
     });

     return false;

    });

    $('.print-toggle').click(function () {

     window.print();

    });

   }

  }, // Home page
  'page_template_page_index': {
   init: function () {
    // JavaScript to be fired on the home page

    var siteOptions = {
     valueNames: ['site-link']
    };

    var siteSearch = new List('site-list', siteOptions);
    var resourceSearch = new List('resource-list', siteOptions);
    var newsSearch = new List('news-list', siteOptions);
    var videosSearch = new List('videos-list', siteOptions);

    $(".search").keyup(function (e) {

     e.stopPropagation();

     siteSearch.search($(this).val());
     resourceSearch.search($(this).val());
     newsSearch.search($(this).val());
     videosSearch.search($(this).val());

     $('.results p span').text($('.main-body .site-link').length);

     $('.search-list').each(function () {

      var hasItems = $(this).find('.site-link').length;
      var id = $(this).attr('id');

      if (hasItems > 0) {

       $(this).slideDown(500);
       $(this).prev('.toggle').addClass('active');
       $('a[href="#' + id + '"]').addClass('visible');

      } else {
       $(this).slideUp(500);
       $(this).prev('.toggle').removeClass('active');
       $('a[href="#' + id + '"]').removeClass('visible');

      }

     });

    });

    $('.toggle').click(function () {

     if ($(this).hasClass('active')) {

      $(this).removeClass('active');
      $(this).next('.search-list').slideUp(500);

     } else {

      $(this).addClass('active');
      $(this).next('.search-list').slideDown(500);

     }

    });

    $('.results a').click(function () {

     var list = $(this).attr('href');
     //                    var list2 = list.replace('#','');

     $(list).prev('.toggle').addClass('active');
     $(list).slideDown('500');

     $('html, body').animate({
      scrollTop: $(list).offset().top - 150
     }, 1000);

     return false;

    });

   },
   finalize: function () {
    // JavaScript to be fired on the home page, after the init JS

   }
  }, // About us page, note the change from about-us to about_us., // Home page
  'page_template_resources': {
   init: function () {
    // JavaScript to be fired on the home page

    $('.toggle').click(function () {

     if ($(this).hasClass('active')) {

      $(this).removeClass('active');
      $(this).next('.search-list').slideUp(500);

     } else {

      $(this).addClass('active');
      $(this).next('.search-list').slideDown(500);

     }

    });

    $('.results a').click(function () {

     var list = $(this).attr('href');
     //                    var list2 = list.replace('#','');

     $(list).prev('.toggle').addClass('active');
     $(list).slideDown('500');

     $('html, body').animate({
      scrollTop: $(list).offset().top - 150
     }, 1000);

     return false;

    });

   },
   finalize: function () {
    // JavaScript to be fired on the home page, after the init JS

    stuckPadding();

    $(window).resize(function () {
     stuckPadding()

    });

    function stuckPadding() {

     var sticky = new Waypoint.Sticky({
      element: $('#float-nav')[0],
      wrapper: false
     })

     //                            var containerWidth = $('body').find('.container').first().outerWidth();
     //                            var windowWidth = $(window).width();
     //                            var half = ((windowWidth - containerWidth) / 2) + 15;

     $("<style type='text/css'>#float-nav.stuck { width: " + $('.col-xl-4.offset-xl-0.col-md-4.offset-md-1.col-11.offset-1').width() + "px}</style>").appendTo('head');

    }

   }
  }, // About us page, note the change from about-us to about_us., // Home page
  'page_template_holders': {
   init: function () {
    // JavaScript to be fired on the home page

    $('.toggle').click(function () {

     if ($(this).hasClass('active')) {

      $(this).removeClass('active');
      $(this).next('.search-list').slideUp(500);

     } else {

      $(this).addClass('active');
      $(this).next('.search-list').slideDown(500);

     }

    });

    $('.results a').click(function () {

     var list = $(this).attr('href');
     //                    var list2 = list.replace('#','');

     $(list).prev('.toggle').addClass('active');
     $(list).slideDown('500');

     $('html, body').animate({
      scrollTop: $(list).offset().top - 150
     }, 1000);

     return false;

    });

   },
   finalize: function () {
    // JavaScript to be fired on the home page, after the init JS

    $(document).ready(function () {

     $('.results p span').text($('.site-link').length);

    })
    stuckPadding();

    $(window).resize(function () {
     stuckPadding()

    });

    function stuckPadding() {

     var sticky = new Waypoint.Sticky({
      element: $('#float-nav')[0],
      wrapper: false
     })

     $("<style type='text/css'>#float-nav.stuck { width: " + $('.col-xl-4.offset-xl-0.col-md-4.offset-md-1.col-11.offset-1').width() + "px}</style>").appendTo('head');

    }

   }
  }, // About us page, note the change from about-us to about_us.
  'page_template_template_faq': {
   init: function () {
    // JavaScript to be fired on the about us page

    //            $(".faq-accordion-title").click(function () {
    //
    //                var accordion = $(this).next('.accordion-wrap');
    //                var toggle = $(this).children('h2').find('.plusminus');
    //
    //                if($(accordion).hasClass('active')) {
    //
    //                    $(toggle).text('+');
    //
    //                } else {
    //
    //                    $(toggle).text('-');
    //                }
    //
    //
    //                $('.accordion-wrap').not(accordion).slideUp(300).removeClass('active');
    //                $(accordion).slideToggle(300).toggleClass('active');
    //
    //            });

    stuckPadding();

    $(window).resize(function () {
     stuckPadding()

    });

    function stuckPadding() {

     var sticky = new Waypoint.Sticky({
      element: $('#float-nav')[0],
      wrapper: false
     })

     $("<style type='text/css'>#float-nav.stuck { width: " + $('.col-xl-4.offset-xl-0.col-md-4.offset-md-1.col-11.offset-1').width() + "px}</style>").appendTo('head');

    }

    $('.toggle').click(function () {

     if ($(this).hasClass('active')) {

      $(this).removeClass('active');
      $(this).next('.accordion-wrap').slideUp(500);

     } else {

      $(this).addClass('active');
      $(this).next('.accordion-wrap').slideDown(500);

     }

    });

    $('.faq-navigation a').click(function () {

     var list = $(this).attr('href');
     //                    var list2 = list.replace('#','');

     $(list).addClass('active');
     $(list).next('.accordion-wrap').slideDown(500);

     $('html, body').animate({
      scrollTop: $(list).offset().top - 80
     }, 1000);

     return false;

    });

   }
  }, // About us page, note the change from about-us to about_us.
  'page_template_default': {
   init: function () {
    // JavaScript to be fired on the about us page

    $('.post-body blockquote, .post-body p').first().each(function () {

     var quote = $(this).html();

     $('.get-quote').append('<blockquote>' + quote + '</blockquote>');
     $(this).remove();

    })

   }
  }, // About us page, note the change from about-us to about_us.
  'page_template_template_history': {
   init: function () {
    // JavaScript to be fired on the about us page

    $('.history .history-content p').first().each(function () {

     var quote = $(this).html();

     $('.about-quote').append('<blockquote>' + quote + '</blockquote>');
     $(this).remove();

    })

   }
  }, // About us page, note the change from about-us to about_us.
  'search': {
   init: function () {
    // JavaScript to be fired on the about us page

    $(document).ready(function () {

     function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
       results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
     }

     var term = getParameterByName('s');
     var regex = new RegExp(term, "i");
     //                var regex = regex + "i";

     //                var term = /term/i;
     console.log(regex);

     $(".search-body ul li a, .search-body ul li p").each(function () {
      var string = $(this).html();

      //                console.log(found);

      if (string.match(regex) != null) {
       var found = string.match(regex)[0];

       $(this).html(string.replace(found, '<b>' + found + '</b>'));

      }

     });
    });

   }
  }
 };
 // The routing fires all common scripts, followed by the page specific scripts.
 // Add additional events for more control over timing e.g. a finalize event
 var UTIL = {
  fire: function (func, funcname, args) {
   var fire;
   var namespace = Sage;
   funcname = (funcname === undefined) ? 'init' : funcname;
   fire = func !== '';
   fire = fire && namespace[func];
   fire = fire && typeof namespace[func][funcname] === 'function';
   if (fire) {
    namespace[func][funcname](args);
   }
  },
  loadEvents: function () {
   // Fire common init JS
   UTIL.fire('common');
   // Fire page-specific init JS, and then finalize JS
   $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
    UTIL.fire(classnm);
    UTIL.fire(classnm, 'finalize');
   });
   // Fire common finalize JS
   UTIL.fire('common', 'finalize');

   $('.loader').delay(500).fadeOut(500);
  }
 };
 // Load Events
 $(document).ready(UTIL.loadEvents);
})(jQuery); // Fully reference jQuery after this point.
