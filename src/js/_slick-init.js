 $(document).ready(function () {
     $('.single-leader').slick({
         arrows: true,
         //     adaptiveHeight:true,
         slidesToShow: 1,
         slidesToScroll: 1,
         centerMode: true,

         focusOnSelect: true,

         responsive: [{
             breakpoint: 769,
             settings: {
                 slidesToShow: 1,
                 centerPadding: '0px',
                 adaptiveHeight: true,



             }
      }]
     });



 });
 // });

 //slick gallery slider
 //used on single leader pages
 $(document).ready(function () {
     $('.photo-slider').slick({

         slidesToShow: 3,
         slidesToScroll: 1,
         centerMode: true,
         focusOnSelect: true,
         dots: false,
         //     variableWidth:true,
         //     variableHeight:true,
         responsive: [
//                      {
//                    breakpoint: 768,
//                    settings: {
//                      arrows: true,
//                      centerMode: true,
//                      centerPadding: '0',
//                      slidesToShow: 3
//                    }
//                  },
             {
                 breakpoint: 768,
                 settings: {
                     arrows: false,
                     centerMode: true,
                     centerPadding: '0',
                     slidesToShow: 1
                 }
                  }]
     });
     $('#hero-slider').slick({
         autoplay: true,
         autoplayspeed: 2000,
         slidesToShow: 1,
         slidesToScroll: 1,
         speed: 1000,
         // index: 2,
         centerMode: true,
         // focusOnSelect:true,
         fade: true,
         dots: true,
//         variableWidth: true,
         responsive: [{
                 breakpoint: 768,
                 settings: {
                     dots: false,
                 }
      }]
             //                   {
             //     //   breakpoint: 480,
             //     //   settings: {
             //     //     arrows: false,
             //     //     centerMode: true,
             //     //     centerPadding: '40px',
             //     //     slidesToShow: 1
             //     //   }
             //     // }]
             //   };
     });
 });
