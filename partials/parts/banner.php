    <section class="page-banner">

<img <?php if(get_field('downloadable')): echo 'class="downloadable"'; endif;?> src="<?php if($banner !== null): echo $banner['sizes']['banner']; else: echo '' . get_template_directory_uri(). '/dist/img/flun-18.jpg'; endif;?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            <div class="container">
                <div class="row">
                <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">
                <h1 role="heading"><?php echo get_the_title();?></h1>

                </div>
                </div>

            </div>

        </section>
