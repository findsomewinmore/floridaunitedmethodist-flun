<?php


   $cats = get_the_category();

   $args = array(

                    'posts_per_page' => 3,
                    'orderby' => 'date' ,
                    'order'   => 'DESC',
                    'post_type' => 'post',
                    'cat'     => $cats[0]->term_id,
                    'post_status' => 'publish'

   );

            $loop = new WP_Query( $args );?>

<?php if ( $loop->have_posts() && has_category() ) :?>

<section class="section-header">
  <div class="container">
    <div class="col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
      <h2 class="back-dash">Related News</h2>
    </div>
  </div>
</section>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php
                $date = get_the_date();
                $title = get_the_title();
                $news_blurb = get_the_excerpt();
                $post_url = get_the_permalink();
                if(get_field('preview_image')): $banner = get_field('preview_image'); else: $banner = get_field('banner'); endif;
                $date =get_the_date('F d, Y');
            ?>

<div class="news" style="background-image: url(<?php echo $banner['sizes']['banner']; ?>);">
  <div class="container">

    <div class="news-container col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
      <h2 class="news_title"><?php echo $title ?></h2>
      <h5 class="text-white serif"><?php echo $date ?></h5>
      <p class="news-blurb"><?php echo $news_blurb ?></p>
      <a class="news-btn btn" href="<?php echo $post_url ?>">Read More</a>
    </div>
  </div>
</div>

<?php endwhile; wp_reset_postdata();?>

<section>
  <div class="container">
    <div class="col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
      <a href="/category/<?php echo $cats[0]->slug;?>" class="btn">See More Related News</a>
    </div>
  </div>

</section>

<?php else: wp_reset_postdata();?>

<section class="section-header">
  <div class="container">
    <div class="col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
      <h2 class="back-dash">News</h2>
    </div>
  </div>
</section>

<?php    $args = array(

                    'posts_per_page' => 3,
                    'orderby' => 'date' ,
                    'order'   => 'DESC',
                    'post_type' => 'post'
   );

            $loop = new WP_Query( $args );?>

<?php while ( $loop->have_posts() ) : $loop->the_post();

                $date = get_the_date();
                $title = get_the_title();
                $news_blurb = get_the_excerpt();
                $post_url = get_the_permalink();
                if(get_field('preview_image')): $banner = get_field('preview_image'); else: $banner = get_field('banner'); endif;
                $date =get_the_date('F d, Y');
            ?>
<div class="news" style="background-image: url(<?php echo $banner['sizes']['banner']; ?>);">
  <div class="container">

    <div class="news-container col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
      <h2 class="news_title"><?php echo $title ?></h2>
      <h5 class="text-white serif"><?php echo $date ?></h5>
      <p class="news-blurb"><?php echo $news_blurb ?></p>
      <a class="news-btn btn" href="<?php echo $post_url ?>">Read More</a>
    </div>
  </div>
</div>

<?php endwhile; wp_reset_postdata();?>

<section>
  <div class="container">
    <div class="col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
      <a href="/category/news" class="btn">See All News</a>
    </div>
  </div>
</section>
<?php endif;?>
