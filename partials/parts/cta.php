<!-- CTA -->
    <div class="cta">
        <div class="fp-cta">
            <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <p class="cta-title">Get Involved Today</p>
                </div>


                    <?php if( have_rows('site_ctas', 'options') ):?>
                    <?php while ( have_rows('site_ctas', 'options') ) : the_row();?>

                            <div class="col-md-2 cta-buttons">
                                <a href="<?php $link = get_sub_field('link'); the_permalink($link->ID);?>" class=""><?php the_sub_field('link_text');?></a>
                            </div>

                    <?php endwhile; ?>
                    <?php endif; ?>

                            <div class="col-md-2 cta-buttons">
                                <a href="#newsletter-modal" class="modal-toggle">Subscribe</a>
                            </div>

            </div>
        </div>
    </div>
    </div>
