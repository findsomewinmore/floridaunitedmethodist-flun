<?php
/*
 * Template Name: History
 * Description: History Page
 */
?>

<?php get_template_part('templates/header'); ?>

    <div class="main-container">
        <?php
            $banner = get_field('history_banner');
            $banner_header = get_field('banner_header');
            $history_blurb = get_field('history_blurb');
            $history_blurb_2 = get_field('history_blurb_2');
            $history_blurb_3 = get_field('history_blurb_3');
            $history_hero_image = get_field('history_hero_image');
            $history_hero_text = get_field('history_hero_text');
            $history_image_long = get_field('history_image_long');
            $history_images = get_field('history_images');
            $our_mission_title = get_field('our_mission_title');
            $our_mission_blurb = get_field('our_mission_blurb');
            $our_mission_image = get_field('our_mission_image');
            $mission_cta = get_field('mission_cta');
            $cta_title = get_field('cta_title');
            $cta = get_field('cta');
        ?>

<!-- History Banner -->

<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>


<!-- History Section -->
<section>

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">
                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => $menu );
                    wp_nav_menu( $menu_args );
                    ?>

            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 about-quote">
                <?php echo $quote ?>
            </div>
        </div>
    </div>


    </section>
    <section class="history">


    <div class="container">

        <div class="row">
            <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-md-8 offset-md-2 history-content">
                <?php echo $history_blurb ?>
            </div>
        </div>

    </div>

    </section>

        <section class="history history-banner">


            <img class="history-hero-img" src="<?php echo $history_hero_image['url']; ?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            <div class="overlay"></div>
            <div class="container">
            <div class="col-md-8 offset-md-2 text-white">
            <?php echo $history_hero_text ?>

            </div>

        </div>

        </section>
        <section class="history">

        <div class="container">

        <div class="row">
            <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <p><?php echo $history_blurb_2 ?></p>
            </div>
        </div>
        </div>

        </section>

        <section class="history p0 mb-3">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
            <img class="history-center-image" src="<?php echo $history_image_long['url']; ?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            </div>
        </div>
        <div class="row">
            <?php foreach($history_images as $key => $history_images){ ?>
                <?php
                    $history_image_1 = $history_images['history_image_1'];
                    $history_image_2 = $history_images['history_image_2'];
                ?>
            <div class="col-md-4 offset-md-2">
                <img class="history-center-image" src="<?php echo $history_image_1['sizes']['large']; ?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            </div>
            <div class="col-md-4">
                <img class="history-center-image" src="<?php echo $history_image_2['sizes']['large']; ?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            </div>


            <?php } ?>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <p><?php echo $history_blurb_3 ?></p>
            </div>
        </div>

    </div>

    </section>

<!-- Mission Section -->

    <section class="history-mission p0">
        <div class="row">
            <div class="col-md-6">
                <div class="content">
                <h2 class="history-mission-title text-white"><?php echo $our_mission_title ?></h2>
                <div class="history-mission-blurb"><?php echo $our_mission_blurb ?></div>
                    </div>
            </div>
            <div class="col-md-6">
                <img class="history-mission-image" src="<?php echo $our_mission_image['url'] ?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            </div>
        </div>
    </section>

<!-- Mission CTA -->

    <section class="mission-cta">
        <div class="container">

        <div class="row">


          <?php if( have_rows('mission_cta') ):?>
          <?php while ( have_rows('mission_cta') ) : the_row();?>




                    <?php
                        $mission_cta_title = get_sub_field('mission_cta_title');
                        $mission_cta_blurb = get_sub_field('mission_cta_blurb');
                        $mission_cta_url = get_sub_field('mission_cta_url');
                        $cta_url_id = get_sub_field('mission_cta_url', false, false);
                        $cta_type = get_post_type($cta_url_id);
                        $mission_cta_button = get_sub_field('mission_cta_button');
                    ?>
                    <div class="col-md-4 mission-cta-block">
                        <h3><?php echo $mission_cta_title ?></h3>

                        <?php echo $mission_cta_blurb ?>
                            <a href="<?php if($cta_type == 'resources'): echo get_field('file', $cta_url_id); else: echo $mission_cta_url; endif; ?>" class="btn" <?php if($cta_type == 'resources'): echo 'target="blank"'; endif;?>><?php echo $mission_cta_button ?></a>
                    </div>

         <?php endwhile; ?>
          <?php endif; ?>

        </div>
        </div>

    </section>

<!-- Bottom CTA -->

    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>


</div>

<?php get_template_part('templates/footer'); ?>
