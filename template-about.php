<?php
/*
 * Template Name: Who We Are
 * Description: About Us Page
 */
?>

<?php get_template_part('templates/header'); ?>


    <?php
        $about_banner = get_field('about_banner');
        $banner_header = get_field('banner_header');
        $navigation = get_field('navigation');
        $quote = get_field('quote');
        $mission_flex_1 = get_field('mission_flex_1');
        $mission_flex_2 = get_field('mission_flex_2');
        $values_title = get_field('values_title');
        $values_repeater_first = get_field('values_repeater_first');
        $values_flex = get_field('values_flex');
        $values_repeater_third = get_field('values_repeater_third');
        $news_title = get_field('news_title');
        $news = get_field('news');
        $cta_title = get_field('cta_title');
        $cta = get_field('cta');
    ?>


<div class="main-container">
    <section class="page-banner">

            <img class="" src="<?php echo $about_banner['url']; ?>" />
            <div class="container">
                <h1 role="heading"><?php echo $banner_header ?></h1>
            </div>

        </section>

<section>

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">
                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => $menu );
                    wp_nav_menu( $menu_args );
                    ?>

            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 about-quote">
                <?php echo $quote ?>
            </div>
        </div>
    </div>


    </section>

<!-- Mission section -->
<?php
  $background_image = get_field( 'background_banner' );

  if( ! $background_image )
  {
    $background_image = get_stylesheet_directory_uri() . '/dist/img/flun-38.jpg';
  }

  $background       = 'style="background: url(' . $background_image . '); background-size: 100% auto; background-position: 50% -25vw; background-repeat: no-repeat;"';
?>
<section class="about-main mt-5" <?php echo $background; ?>>
    <div class="container">
        <?php foreach($mission_flex_1 as $key => $mission){ ?>
            <?php
                $mission_title = $mission['mission_title'];
                $mission_section_2 = $mission['mission_section_2'];
                $mission_image = $mission['mission_image']
            ?>
            <div class ="row mission-row align-items-end">
            <div class="col-xl-6 col-lg-5 offset-1">
                <h2 class="back-dash"><?php echo $mission_title ?></h2>
                <div class="mission-text"><?php echo $mission_section_2 ?></div>
            </div>
            <div class="col-lg-4 offset-lg-1">
                <?php echo $mission_image;?>
            </div>
        </div>
        <?php } ?>
        <?php foreach($mission_flex_2 as $key => $mission_2){?>
            <?php
                $mission_image = $mission_2['mission_image'];
                $mission_section_2 = $mission_2['mission_section_2'];
            ?>
            <div class="row mission-row mission-section-2">
                <div class="col-xl-4 col-lg-4 offset-lg-1">
                    <img class="mission-img" src="<?php echo $mission_image['sizes']['medium']; ?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
                </div>
                <div class="col-lg-6 offset-lg-1">
                    <div class="mission-text-2"><?php echo $mission_section_2 ?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<!-- Values Section -->

    <div class="values-about pt-5">
        <div class="container">
                <h2 class="back-dash"><?php echo $values_title ?></h2>
        <div class="row">

            <?php foreach($values_repeater_first as $key => $values){ ?>
                <?php
                    $values_section_title = $values['values_section_title'];
                    $values_blurb = $values['values_blurb'];
                ?>
            <div class="col-lg-4 values-section">
                <div>
                <h4 class="values-title"><?php echo $values_section_title ?></h4>
                <div class="values-about-text"><?php echo $values_blurb ?></div>
                    </div>
            </div>
                <?php } ?>
        </div>

        <div class="row">

            <?php foreach($values_flex as $key => $values_flex){?>
                <?php
                    $values_image = $values_flex['values_image'];
                    $values_image2 = $values_flex['values_image_2'];
                    $values_section_title = $values_flex['values_section_title'];
                    $values_blurb = $values_flex['values_blurb']
                ?>
            <div class="col-lg-4 values-flex values-section">
                <div class="image" style="background-image: url(<?php echo $values_image['sizes']['large'] ?>); background-size: cover; background-position: 50%;">

                </div>
            </div>
            <div class="col-lg-4 values-section">
                <div>
                <h4 class="values-title"><?php echo $values_section_title ?></h4>
                <div class="values-about-text"><?php echo $values_blurb ?></div>
                </div>
            </div>
            <div class="col-lg-4 values-flex values-section">
                <div class="image" style="background-image: url(<?php echo $values_image2['sizes']['large'] ?>); background-size: cover; background-position: 50%;">
                </div>
            </div>
            <?php } ?>
        </div>

        <div class="row">

            <?php foreach($values_repeater_third as $key => $values_3){ ?>
                <?php
                    $values_section_title = $values_3['values_section_title'];
                    $values_blurb = $values_3['values_blurb'];
                ?>
            <div class="col-lg-4 values-section">
                <div>
                <h4 class="values-title"><?php echo $values_section_title ?></h4>
                <div class="values-about-text"><?php echo $values_blurb ?></div>
                </div>
            </div>
                <?php } ?>
        </div>
        </div>


    </div>

    <?php include( locate_template( 'partials/parts/recent-news.php', false, false ) );?>


    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>



<?php get_template_part('templates/footer'); ?>
</div>
