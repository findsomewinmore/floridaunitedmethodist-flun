<?php
/** Template Name: Index
 * The Template for displaying all single posts
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>
<!-- Banner -->



<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>

<section class="search-input">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 offset-xl-0 col-md-11 offset-md-1 col-11 offset-1">
                    <input class="search fuzzy-search" id="search" placeholder="search">


            </div>

        </div>
    </div>
    </section>
<section class="main-body">
    <div class="container">

        <div class="row">

        <div class="col-xl-4 offset-xl-0 col-md-4 offset-md-1 col-11 offset-1">

            <div id="float-nav" class="results">
            <p><span>0</span> Results</p>
            <a href="#site-list">Pages</a>
            <a href="#resource-list">Resources</a>
            <!-- <a href="#news-list">News</a> -->
            <a href="#video-list">Videos</a>

            </div>


        </div>


        <div class="col-xl-8 offset-xl-0 col-md-7 offset-md-0 col-11 offset-1">

            <h2 class="line toggle">Pages<span>+</span></h2>
            <div class="search-list" id="site-list">
            <input class="search hidden" placeholder="search">

            <ul class="index-list list" role="listbox">

                       <?php
                       $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'title' ,
                                        'order'   => 'ASC',
                                        'post_type' => array( 'page', 'eventlistings', 'ministry' ),
                                        'meta_query' => array(
                                          'meta_key' => 'exclude_from_az_index',
                                          'meta_value' => 'Yes',
                                          'meta_compare' => '!=')
                       );

                                $loop = new WP_Query( $args );?>

                            <?php if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>

                                   <?php if(get_field('exclude_from_az_index')) continue;?>
                                    <li><a class="site-link" href="<?php the_permalink();?>"><?php echo get_the_title();?></a></li>


                            <?php endwhile; endif; wp_reset_postdata(); ?>

                       <?php

                       $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'title' ,
                                        'order'   => 'ASC',
                                        'post_type' => 'team_members',
                                        'cat' => 7,
                                        'meta_query' => array(
                                          'meta_key' => 'exclude_from_az_index',
                                          'meta_value' => 'Yes',
                                          'meta_compare' => '!=')
                       );

                                $loop = new WP_Query( $args );?>

                            <?php if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                   <?php if(get_field('exclude_from_az_index')) continue;?>
                                    <li><a class="site-link" href="<?php the_permalink();?>"><?php echo get_the_title();?></a></li>


                            <?php endwhile; endif; wp_reset_postdata(); ?>

                </ul>
            </div>

            <h2 class="line toggle">Resources<span>+</span></h2>
            <div class="search-list" id="resource-list">
            <input class="search hidden" placeholder="search">
            <ul class="index-list list" role="listbox">


                       <?php

                       $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'title' ,
                                        'order'   => 'ASC',
                                        'post_type' => 'resources',
                                        'meta_query' => array(
                                          'meta_key' => 'exclude_from_index',
                                          'meta_value' => 'Yes',
                                          'meta_compare' => '!=')
                       );

                                $loop = new WP_Query( $args );?>

                     <?php if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>
                            <?php if(get_field('exclude_from_index')) continue;?>
                            <li><a class="site-link downloadable" href="<?php the_field('file', $report->ID);?>"><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                 <?php endwhile; endif; wp_reset_postdata(); ?>



                </ul>
            </div>

            <!-- <h2 class="line toggle">News<span>+</span></h2> -->
            <div class="search-list" id="news-list">
            <input class="search hidden" placeholder="search">
            <ul class="index-list list" role="listbox" id="news-list">


                       <?php

                       $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'title' ,
                                        'order'   => 'ASC',
                                        'post_type' => 'post',
                                        'cat' => 9
                                        );

                                $loop = new WP_Query( $args );?>

                            <?php if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); if(!get_field('exclude_from_az_index')):?>

                                    <li><a class="site-link" href="<?php the_permalink();?>"><?php echo get_the_title();?></a></li>


                            <?php endif; endwhile; endif; wp_reset_postdata(); ?>

                </ul>
            </div>

            <h2 class="line toggle">Videos<span>+</span></h2>
            <div class="search-list" id="videos-list">
            <input class="search hidden" placeholder="search">
            <ul class="index-list list" role="listbox" id="news-list">


                       <?php

                       $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'title' ,
                                        'order'   => 'ASC',
                                        'post_type' => 'videos',
                                        );

                                $loop = new WP_Query( $args );?>

                            <?php if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>

                                    <li><a class="site-link" href="<?php the_permalink();?>"><?php echo get_the_title();?></a></li>


                            <?php endwhile; endif; wp_reset_postdata(); ?>

                </ul>

            </div>

        </div>


        </div>




    </div>


</section>

    <?php include( locate_template( 'partials/parts/recent-news.php', false, false ) );?>


    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>


</div>
<?php get_template_part('templates/footer'); ?>
