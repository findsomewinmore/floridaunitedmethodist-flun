<?php
/**
 * Template Name: Account Holders
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>
<!-- Banner -->



<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>

<section class="main-body">
    <div class="container">

        <div class="row">

        <div class="col-xl-4 offset-xl-0 col-md-4 offset-md-1 col-11 offset-1">

            <div id="float-nav" class="results resources">
            <?php if( have_rows('account_sectons') ):?>
            <?php while ( have_rows('account_sectons') ) : the_row();?>

            <a href="#<?php $var = sanitize_title_for_query(get_sub_field('title') ); echo esc_attr( $var);?>"><?php the_sub_field('title');?></a>
            <?php endwhile; ?>
            <?php endif; ?>

            </div>


        </div>


        <div class="col-xl-8 offset-xl-0 col-md-7 offset-md-0 col-11 offset-1">


            <?php if( have_rows('account_sectons') ):?>
            <?php while ( have_rows('account_sectons') ) : the_row();?>

            <h2 class="line toggle"><?php the_sub_field('title');?><span>+</span></h2>
            <div class="search-list" id="<?php $var = sanitize_title_for_query(get_sub_field('title') ); echo esc_attr( $var);?>">

                <?php the_sub_field('content');?>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>


        </div>


        </div>




    </div>


</section>

<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>


</div>
