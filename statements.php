<?php
/**
 * Template Name: Reports
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>
<!-- Banner -->



<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>


    <section>

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">
                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => $menu );
                    wp_nav_menu( $menu_args );
                    ?>

            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0 about-quote">
                <blockquote>Each year, the foundation produces an annual report to highlight the many ways churches, individuals and agencies are partnering with the foundation to put faithful stewardship into action. Take a look at what’s been accomplished and learn how you can be part of this important ministry.</blockquote>

            </div>
        </div>
    </div>


    </section>
<section class="p0">
       <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-4 col-lg-7 offset-lg-3 post-body">


                               <ul class="downloads">



                <?php

                if(get_field('report_type')): $type = get_field('report_type');

                if(get_field('' . $type . '', 'options')):

//                echo $type;

                $reports = get_field('' . $type . '', 'options');

                if($type == 'annual_reports'):
                foreach($reports as $report):?>
                <li><a class="downloadable" href="<?php the_field('file', $report->ID);?>" ><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                <?php endforeach;?>


                <?php else:?>

                <?php if( have_rows('' . $type . '', 'options') ):?>


                <?php while ( have_rows('' . $type . '', 'options') ) : the_row();?>
                <p><?php the_sub_field('form_category');?></p>
                <ul class="downloads">

                <?php if(get_sub_field('forms')): $reports = get_sub_field('forms');

                foreach($reports as $report):?>


                <li><a class="downloadable" href="<?php the_field('file', $report->ID);?>" ><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                <?php endforeach; endif;


                ?>

                </ul>
                    <?php if(get_sub_field('additional_links')):?>
                    <?php the_sub_field('additional_links');?>
                    <?php endif;?>

                <?php endwhile; ?>

                <?php endif; ?>

                <?php endif;?>


                <?php endif; endif; ?>

                </ul>




            </div>

        </div>
    </div>
</section>

<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>


</div>
