<?php
/*
 * Template Name: FAQ
 * Description: Frequently Asked Questions
 */
?>


<?php get_template_part('templates/header'); ?>

    <?php
        $faq_banner = get_field('faq_banner');
        $navigation = get_field('navigation');
        $banner_header = get_field('banner_header');
        $faq_sections = get_field('faq_sections');
    ?>

<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo $faq_banner['sizes']['banner']; ?>" />
            <div class="container">
                <h1 role="heading"><?php echo get_the_title();?></h1>
            </div>

        </section>

    <section>

<div class="faq-body container">
<div class="row">
    <div class="col-xl-4 offset-xl-0 col-md-4 offset-md-1 col-11 offset-1">
        <div id="float-nav" class="faq-navigation">
            <?php echo $navigation ?>
        </div>

    </div>


<div class="col-xl-8 offset-xl-0 col-md-7 offset-md-0 col-11 offset-1">

    <div class="faq-section">
    <!-- Church Loans -->

            <h2 class="line toggle" id="church-loans">Church Loans<span>+</span></h2>
            <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'menu_order' ,
                                        'order'   => 'ASC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Church Loans'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>


            <?php endwhile; endif; wp_reset_postdata();?>

            </div>
    <!-- Development Fund Accounts -->

            <h2 class="line toggle" id="development-funds">Development Fund Accounts<span>+</span></h2>


            <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'menu_order' ,
//                                        'order'   => 'DESC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Development Fund Accounts'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>


                <?php endwhile; endif; wp_reset_postdata();?>
            </div>
    <!-- Endowments -->
            <h2 class="line toggle" id="endowments">Endowments<span>+</span></h2>


            <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'date' ,
                                        'order'   => 'DESC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Endowments'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>


            <?php endwhile; endif; wp_reset_postdata();?>
            </div>

    <!-- Investment Funds -->
            <h2 class="line toggle" id="investment-funds">Investment Funds (FUMF Funds Accounts)<span>+</span></h2>

            <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'date' ,
                                        'order'   => 'DESC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Investment Funds (FUMF Funds Accounts)'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>


            <?php endwhile; endif; wp_reset_postdata();?>
        </div>

    <!-- Planned Giving -->
            <h2 class="line toggle" id="planned-giving">Planned Giving<span>+</span></h2>


        <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'date' ,
                                        'order'   => 'DESC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Planned Giving'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>


            <?php endwhile; endif; wp_reset_postdata();?>

            </div>

    <!-- Scholarships -->
            <h2 class="line toggle" id="scholarships">Scholarships<span>+</span></h2>


        <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'date' ,
                                        'order'   => 'DESC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Scholarships'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>

            <?php endwhile; endif; wp_reset_postdata();?>
          </div>

    <!-- Student Loans -->

            <h2 class="line toggle" id="student-loans">Student Loans<span>+</span></h2>


        <div class="accordion-wrap">
                     <?php

                                $args = array(

                                        'posts_per_page' => -1,
                                        'orderby' => 'date' ,
                                        'order'   => 'DESC',
                                        'post_type' => 'qa_faqs',
                                        'meta_key' => 'faq_category',
                                        'meta_value' => 'Student Loans'
                                        );

                                $loop = new WP_Query( $args );

                                if( $loop->have_posts() ): while ( $loop->have_posts() ) : $loop->the_post(); ?>


            <div class="accordion">


                <div class="faq-accordion-container" >
                        <h4 class="faq-question" ><?php echo get_the_title() ?></h4>
                        <div class="faq-answer"><?php the_content(); ?></div>
                </div>

            </div>


            <?php endwhile; endif; wp_reset_postdata();?>
    </div>

</div>

    </div>
</div>



    </div>
</section>

<?php include( locate_template( 'partials/parts/recent-news.php', false, false ) );?>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>
</div>
