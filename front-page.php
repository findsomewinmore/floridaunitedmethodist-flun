<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header');?>

<div class="home-container">

<?php
     $banner_content = get_field('banner_content');
     $banner_text = get_field('banner_text');
     $intro_title = get_field('intro_title');
     $intro_text = get_field('intro_text');
     $info_pane = get_field('info_pane');
     $cta_title = get_field('cta_title');
     $cta = get_field('cta');
     $two_column_title = get_field('two_column_title');
     $two_column_text = get_field('two_column_text');
     $two_column_image = get_field('two_column_image');
     $news = get_field('news');
     ?>

<!-- Banner -->
     <section class="banner-home p0">
     <!-- <div class="row"> -->
         <?php
        if( $banner_content ): ?>
        <div id="hero-slider">
                <?php foreach( $banner_content as $image ): ?>


                    <div class="item slides" style="background-image: url(<?php echo $image['url']; ?>);">
                        <?php if(get_field('banner_video', false, false)):?><iframe width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
                        <a href="#" class="video-close"></a> <?php endif;?>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 offset-xl-0 col-lg-8 offset-lg-2">
                                    <div class="banner-text">
                                        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/logo.svg">
                                        <?php echo $banner_text ?>
                                            <?php $video = get_field('banner_video', false, false);
                                                    $videoID = substr($video, strpos($video, "=") + 1);
                                                    $doc = new DOMDocument();
                                                    $internalErrors = libxml_use_internal_errors(true);
                                                    $doc->preserveWhiteSpace = FALSE;
                                                    $doc->loadHTMLFile($video);

                                                    $title_div = $doc->getElementById('eow-title');
                                                    $vidtitle = $title_div->nodeValue;


                                                                                 ?>

                                        <?php if(get_field('banner_video', false, false)):?>
                                                <a href="https://www.youtube.com/embed/<?php echo $videoID;?>?rel=0&amp;showinfo=0&autoplay=1&enablejsapi=1" class="btn home-video"><i class="fa fa-play"></i>Watch: <?php echo $vidtitle;?></a>

                                        <?php endif;?>
                                    </div>

                                </div>


                            </div>


                        </div>

                    </div>
                <?php endforeach; ?>


        </div>
        <?php endif; ?>
    </section>

<!-- Interest Rate Section -->
    <section class="interest">
        <div class="container">
            <div class="row">
                 <div class="two-col-info col-xl-5 col-lg-4 offset-lg-1">
                     <h2 class="serif"><?php echo $intro_title; ?></h2>
                     <?php echo $intro_text;?>
                     <?php if(get_field('home_button_link_1')):
                     $link = get_field('home_button_link_1');
                     echo '<a class="btn" href="' . get_permalink($link->ID) . '">' . get_field('home_button_text_1') . '</a>';
                     endif;
                     ?>
                 </div>
                    <div class="two-col-pane col-lg-5 offset-lg-1">
                        <?php foreach($info_pane as $key => $info_pane){ ?>
                        <?php
                                $info_content = $info_pane['info_content'];
                                $info_sub_content = $info_pane['info_sub_content'];
                        ?>
                        <div class="info_pane">
                            <h4 class="info_content"> <?php echo $info_content; ?> </h4>
                            <h2 class="info_sub_content"><?php echo $info_sub_content ?></h2>
                        </div>
                            <hr>
                        <?php } ?>
                    </div>
            </div>
        </div>
    </section>


    <?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>

<!-- Values Section	 -->
    <section class="values">
        <div class="container">
            <div class="row">
                <div class="values col-xl-5 col-lg-5 offset-lg-1">
                    <h2 class="fp-title back-dash"><?php echo $two_column_title ?></h2>
                    <div class="values-text">
                    <?php echo $two_column_text ?>
                     <?php if(get_field('home_button_link_2')):
                     $link = get_field('home_button_link_2');
                     echo '<a class="btn" href="' . get_permalink($link->ID) . '">' . get_field('home_button_text_2') . '</a>';
                        endif;
                     ?>


                    </div>
                </div>
                <div class="col-xl-5 offset-xl-1 col-lg-5 offset-lg-1 values-image-column">
                    <?php foreach($two_column_image as $key => $two_column_img){ ?>
                        <?php
                             $image_1 = $two_column_img['image_1'];
                        ?>
                    <div class="values-img">
                        <img src="<?php echo $image_1['url']?>" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<!-- News-->

    <?php include( locate_template( 'partials/parts/recent-news.php', false, false ) );?>

    <?php $args = array(

        'posts_per_page' => 3,
        'orderby' => 'date' ,
        'order'   => 'DESC',
        'post_type' => 'eventlists',
        'post_status' => 'publish'


);

    $loop = new WP_Query( $args );


            if ($loop->have_posts()):?>
    <div class="events-container">
    <section class="section-header">
        <div class="container">
            <div class="row">

                <div class="col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">

                    <h2 class="back-dash">Upcoming Events and Announcements</h2>

                </div>
            </div>
        </div>
    </section>

    <section class="news_section events-front p0">

    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                <?php

                    $title = get_the_title();
                    $news_blurb = get_the_excerpt();
                    $post_url = get_the_permalink();

                    // $link_text = $news_items['link_text'];
                    if(get_field('preview_image')): $banner = get_field('preview_image'); else: $banner = get_field('banner'); endif;


            $temp_date = get_post_meta( get_the_ID(), 'date', true );


            if (strtotime($temp_date) > strtotime('now')){

                if ( $temp_date != $current_header  ) {


                ?>


                    <div class="news" style="background-image: url(<?php echo $banner['sizes']['banner']; ?>)">
                        <div class="container">
                            <div class="news-container col-xl-9 offset-xl-1 col-lg-8 offset-lg-2">
                                <h2 class="news_title"><?php echo $title ?></h2>
                                <p class = "news-blurb"><?php echo $news_blurb ?></p>
                                <a class="news-btn btn" href="<?php echo $post_url ?>">Read More</a>
                            </div>
                        </div>
                    </div>

    <?php }; }; endwhile; wp_reset_postdata();?>
        </section>
    <section>
            <div class="container">
                <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2">
                    <a href="/events-announcements" class="btn">See All Events/Announcements</a>
            </div>
            </div>
        </section>
    </div>
    <?php endif;?>

    <?php get_template_part('templates/footer'); ?>
</div>
