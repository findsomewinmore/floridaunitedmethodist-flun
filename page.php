<?php
/**
 * The Template for displaying all single posts
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>
<!-- Banner -->



<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>

    <?php if(!get_field('remove_nav_quote')):?>

    <section class="pb-0">

    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-0 col-lg-3 offset-lg-1 about-nav">

                <!--<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>-->
                <?php //echo do_shortcode('[fumf-childpages]');?>

                <?php $menu = get_field('page_menu');?>

                    <?php
                    $menu_args = array('menu' => $menu );
                    wp_nav_menu( $menu_args );
                    ?>


                <?php if(get_field('resources') || get_field('addn_resources')):?>
                    <h5 class="mt-3 serif">Resources You Might Be Interested In:</h5>
                <ul class="resource-list">
                <?php $resources = get_field('resources');?>

                <?php foreach($resources as $resource):?>

                <li><a href="<?php the_field('file', $resource->ID);?>" target="_blank" class="downloadable"><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($resource->ID);?></a></li>

                <?php endforeach;?>
                </ul>
                <div class="resource-links">
                <?php the_field('addn_resources');?>
                </div>
                <?php endif;?>
            </div>
            <div class="col-xl-8 offset-xl-0 col-lg-7 offset-lg-0">
                <div class="get-quote"></div>
            </div>
        </div>
    </div>


    </section>
    <?php endif;?>

    <section class="">



    <?php
    $images = get_field('page_gallery');

    if( $images ): ?>

         <div class="photo-slider center mb-3">

        <?php foreach( $images as $image ): ?>

             <div class="photo-slider__item">
                 <img src="<?php echo $image['url']; ?>" />
                 <div>
                                  <p><?php echo $image['caption'];?></p>

                 </div>
             </div>

         <?php endforeach; ?>
     </div>
    <?php endif; wp_reset_postdata(); ?>


       <div class="container">
        <div class="row">
            <div class="col-xl-9 offset-xl-1 col-lg-8 offset-lg-2 post-body">

            <?php
                if (have_posts()) :
                   while (have_posts()) :
                      the_post();
                         the_content();
                   endwhile;
                endif;
            wp_reset_postdata(); ?>

                <?php the_field('script');?>



            </div>

        </div>
    </div>
</section>

<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>

<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>

<?php get_template_part('templates/footer'); ?>


</div>


