<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>


<div class="main-container">
    <section class="page-banner">

        <img src="<?php echo get_template_directory_uri(); ?>/dist/img/flun-5.jpg" alt="<?php echo get_the_title() ?> - <?php bloginfo( 'name' ); ?>"/>
            <div class="container">
        <div class="row">
            <div class="col-xl-9 offset-xl-1 col-md-8 offset-md-2 post-body">
                <h1 class="single-info">Lost?</h1>
                <hr class="leader-line">
                <p class="single-title text-center text-white"><a href="/">Find your way back home...</a></p>
            </div>
            </div>

        </section>


<?php get_template_part('templates/footer'); ?>
        </div>>
