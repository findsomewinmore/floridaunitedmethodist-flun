<?php
/**
 * The Template for displaying all single posts
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $video = get_field('url', false, false);
//    $videoID = substr($video, strpos($video, "=") + 1);
    $doc = new DOMDocument();
    $internalErrors = libxml_use_internal_errors(true);
    $doc->preserveWhiteSpace = FALSE;
    $doc->loadHTMLFile($video);

    $desc_div = $doc->getElementById('watch-description-text');
    $viddesc = $doc->saveHTML($desc_div);
//    $videoCount = $doc->getElementById('watch8-sentiment-actions');
//    $count = $doc->nodeValue($videoCount);

?>
<!-- Banner -->

<div class="main-container">
        <section class="page-banner video">

                    <div class="iframe-wrapper">
                        <?php echo get_field('url');?>

                    </div>



                <div class="container pt-5 pb-5">
                    <div class="row">
                        <div class="col-sm-9">
                         <h1 role="heading"><?php echo get_the_title();?></h1>
                         <h3 class="serif mt-3"><?php echo get_the_date();?></h3>

                            <?php if(get_field('description')): echo get_field('description'); else:

                            $viddesc = str_replace("â",'"', $viddesc);
                            $viddesc = str_replace('"s', "'s", $viddesc);
                            $viddesc = str_replace('"¢', "•", $viddesc);

                            echo $viddesc; endif;
                            ?>



                    </div>
                        <div class="col-sm-3 text-right">

                            <h2><a href="https://www.youtube.com/channel/UCeNsJSaBsLw090tgIoHG1Cg" target="_blank"><i class="fa fa-youtube"></i> Visit Our YouTube Channel for More</a></h2>

                    </div>
                    </div>

                </div>


        </section>




<section class="">

       <div class="photo-slider center video-slider">

            <?php
                  // set up or arguments for our custom query
                  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                  $query_args = array(
                    'post_type' => 'videos',
                    'posts_per_page' => -1,
                      'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged
                  );
                  // create a new instance of WP_Query
                  $the_query = new WP_Query( $query_args );
                ?>

                <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>

                <?php $video = get_field('url', false, false);

                $id = substr($video, strrpos($video, '/') + 1);?>


                <div class="photo-slider__item">
                                    <?php if(get_field('poster')):?>
                                    <img src="<?php echo get_field('poster')['sizes']['large'];?>">
                                    <?php else:?>
                                    <img src="https://img.youtube.com/vi/<?php echo $id;?>/hqdefault.jpg">

                                    <?php endif;?>
                    <div>
                        <p>
                            <?php echo get_the_title();?>
                        </p>
                        <a href="<?php the_permalink();?>" class="btn">View Video</a>

                    </div>
                </div>
              <?php endwhile; endif;?>

     </div>



</section>

<div class="share-bar">
<p>Share</p>
<ul>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>"><i class="fa fa-facebook"></i></a></li>
    <li><a href="https://twitter.com/home?status=<?php the_permalink();?>"><i class="fa fa-twitter"></i></a></li>
    <li><a href="https://plus.google.com/share?url=<?php the_permalink();?>"><i class="fa fa-google-plus"></i></a></li>
    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>"><i class="fa fa-linkedin"></i></a></li>
    <li><a href="mailto:?subject=<?php echo get_the_title();?>&body=Check%20out%20this%20link%20<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/envelope.svg"></a>
    <li class="hidden-sm-down"><a href="#" class="print-toggle"><img src="<?php echo get_template_directory_uri(); ?>/dist/img/print.svg"></a></li>
</ul>

</div>
<?php get_template_part('templates/footer'); ?>
    </div>
