<?php
//Example custom post type

$properties = new CPT(
    array(
        'post_type_name' => 'videos',
        'singular'       => 'Video',
        'plural'         => 'Videos',
        'slug'           => 'videos',
    ),
    array(
        'supports' => array(
            'title',
        ),
        'public' => true,
        'show_ui' => true,
        'taxonomies'          => array(),
        'has_archive' => true

    )
);

$properties->menu_icon("dashicons-format-video");
