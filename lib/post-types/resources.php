<?php
//Example custom post type

$properties = new CPT(
    array(
        'post_type_name' => 'resources',
        'singular'       => 'Resource',
        'plural'         => 'Resources',
        'slug'           => 'resources',
    ),
    array(
        'supports' => array(
            'title',
        ),
        'public' => false,
        'show_ui' => true,
        'taxonomies'          => array(),

    )
);

$properties->menu_icon("dashicons-analytics");
