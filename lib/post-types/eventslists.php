<?php
//Example custom post type

$properties = new CPT(
    array(
        'post_type_name' => 'eventlists',
        'singular'       => 'Event',
        'plural'         => 'Events',
        'slug'           => 'events',
    ),
    array(
        'supports' => array(
            'title', 'editor'
        ),
        'public' => true,
        'show_ui' => true,
        'taxonomies'          => array(),

    )
);

$properties->menu_icon("dashicons-calendar-alt");
