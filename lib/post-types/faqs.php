<?php
//Example custom post type

$properties = new CPT(
    array(
        'post_type_name' => 'qa_faqs',
        'singular'       => 'FAQ',
        'plural'         => 'FAQs',
        'slug'           => 'faqs',
    ),
    array(
        'supports' => array(
            'title', 'editor'
        ),
        'public' => false,
        'show_ui' => true,
        'taxonomies'          => array(  ),

    )
);

$properties->menu_icon("dashicons-info");
