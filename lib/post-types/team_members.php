<?php
//Example custom post type

$properties = new CPT(
    array(
        'post_type_name' => 'team_members',
        'singular'       => 'Team Member',
        'plural'         => 'Team Members',
        'slug'           => 'team-members',
    ),
    array(
        'supports' => array(
            'title',
        ),
        'public' => true,
        'show_ui' => true,
        'taxonomies'          => array(),

    )
);

$properties->menu_icon("dashicons-admin-users");
