<?php
/**
 * Template Name: Resources
 *
 * @package Smores
 * @since Smores 2.0
 */
?>

<?php get_template_part('templates/header'); ?>
<?php
    $banner = get_field('banner');
    $banner_text = get_field('banner_text');
?>
<!-- Banner -->



<div class="main-container">
<?php include( locate_template( 'partials/parts/banner.php', false, false ) );?>

<section class="main-body">
    <div class="container">

        <div class="row">

        <div class="col-xl-4 offset-xl-0 col-md-4 offset-md-1 col-11 offset-1">

            <div id="float-nav" class="results resources">
            <a href="#request">Request Financial Statements, Offering Circulars or Tax Return</a>
            <a href="#site-list">Annual Reports</a>
            <a href="#resource-list">Investment and Financial Information</a>
            <a href="#news-list">Applications and Forms</a>
            <a href="#videos-list">Educational and Promotional Materials</a>
            </div>


        </div>


        <div class="col-xl-8 offset-xl-0 col-md-7 offset-md-0 col-11 offset-1">

            <p class="disclaimer">Please make sure you have the latest version of <a href="https://acrobat.adobe.com/us/en/acrobat/pdf-reader.html" target="_blank">Adobe Reader</a> for best viewing of all documents.</p>

            <h2 class="line toggle">Request Financial Statements, Offering Circulars or Tax Return<span>+</span></h2>
            <div class="search-list" id="request">
            <input class="search hidden" placeholder="search">
                <ul class="index-list list" role="listbox" id="request">
                </ul>
                <?php echo do_shortcode('[gravityform id="10" title="false" description="false" ajax="true"]');?>

            </div>

            <h2 class="line toggle">Annual Reports<span>+</span></h2>
            <div class="search-list" id="site-list">
            <input class="search hidden" placeholder="search">

            <ul class="index-list list" role="listbox">


                <?php if(get_field('annual_reports', 'options')): $reports = get_field('annual_reports', 'options');

                foreach($reports as $report):?>


                <li><a class="downloadable" href="<?php the_field('file', $report->ID);?>" ><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                <?php endforeach; endif;


                ?>


                </ul>
            </div>

            <h2 class="line toggle">Investment and Financial Information<span>+</span></h2>
            <div class="search-list" id="resource-list">
            <input class="search hidden" placeholder="search">
            <ul class="index-list list" role="listbox">


                        <?php if( have_rows('financial_statements', 'options') ):?>


                        <?php while ( have_rows('financial_statements', 'options') ) : the_row();?>

                        <?php if(get_sub_field('forms')): $reports = get_sub_field('forms');

                        foreach($reports as $report):?>


                        <li><a class="downloadable" href="<?php the_field('file', $report->ID);?>" ><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                        <?php endforeach; endif;


                        ?>

                            <?php if(get_sub_field('additional_links')):?>
                            <?php the_sub_field('additional_links');?>
                            <?php endif;?>
                        <?php endwhile; ?>
                        <?php endif; ?>



                </ul>
            </div>

            <h2 class="line toggle">Applications and Forms<span>+</span></h2>
            <div class="search-list" id="news-list">
            <input class="search hidden" placeholder="search">
            <ul class="index-list list" role="listbox" id="news-list">


                <?php if( have_rows('applications_and_forms', 'options') ):?>


                <?php while ( have_rows('applications_and_forms', 'options') ) : the_row();?>

                <?php if(get_sub_field('forms')): $reports = get_sub_field('forms');

                foreach($reports as $report):?>


                <li><a class="downloadable" href="<?php the_field('file', $report->ID);?>" ><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                <?php endforeach; endif;


                ?>
                    <?php if(get_sub_field('additional_links')):?>
                    <?php the_sub_field('additional_links');?>
                    <?php endif;?>

                <?php endwhile; endif;?>

                </ul>

            </div>
            <h2 class="line toggle">Educational and Promotional Materials<span>+</span></h2>
            <div class="search-list" id="videos-list">
            <input class="search hidden" placeholder="search">
            <ul class="index-list list" role="listbox" id="news-list">


                <?php if( have_rows('educational_and_promotional_materials', 'options') ):?>


                <?php while ( have_rows('educational_and_promotional_materials', 'options') ) : the_row();?>
                                                <li class="title"><?php the_sub_field('form_category');?></li>

                <?php if(get_sub_field('forms')): $reports = get_sub_field('forms');

                foreach($reports as $report):?>


                <li><a class="downloadable" href="<?php the_field('file', $report->ID);?>" ><i class="fa fa-file-pdf-o"></i> <?php echo get_the_title($report->ID);?></a></li>

                <?php endforeach; endif;


                ?>
                    <?php if(get_sub_field('additional_links')):?>
                    <?php the_sub_field('additional_links');?>
                    <?php endif;?>

                <?php endwhile; ?>
                <?php endif; ?>

                </ul>

            </div>



        </div>


        </div>




    </div>


</section>

<?php include( locate_template( 'partials/parts/related-stories.php', false, false ) );?>
<?php include( locate_template( 'partials/parts/cta.php', false, false ) );?>
<?php get_template_part('templates/footer'); ?>


</div>
