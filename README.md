# [Oldfashioned](https://bitbucket.org/findsomewinmore/oldfashioned/overview)

Oldfashioned a legacy fork of S'mores - a WordPress starter theme based on Bootstrap 3 or [HTML5 Boilerplate](http://html5boilerplate.com/) & [Foundation](http://foundation.zurb.com) that will help the FiWi Creative Team (and others) create better Wordpress themes.

Oldfashioned is an on-going project and an ever evolving repository. As our systems and processees change, so will Oldfashioned.

## Features

* [Gulp](http://gruntjs.com) for compiling Sass to CSS, checking for JS errors, live reloading, concatenating and minifying files, optimizing PNGs and JPEGs, versioning assets, and generating lean Modernizr builds
* [NPM](http://npmjs.com/) for front-end package management
* [HTML5 Boilerplate](http://html5boilerplate.com/)
  * We use a little older version of HTML5 Boilerplate. We still like Paul Irish's IE version detection. Hey, in IE's case, browser detection _is_ feature detection. 
  * The latest [jQuery](http://jquery.com/) via Google CDN, with a local fallback (Bower)
  * The latest [Modernizr](http://modernizr.com/) build for feature detection, with lean builds with Grunt
  * An optimized Google Analytics snippet
* [Foundation 6](http://foundation.zurb.com)
* [Babel](http://babeljs.io)
* [Autoprefixer](https://github.com/postcss/autoprefixer)
* Organized file and asset structure

## Installation

1. Clone the git repo - `git clone git@bitbucket.org:findsomewinmore/oldfashioned.git`
2. Rename the directory to the name of your theme or website.
3. Remove the .git directory (This will preven you from commiting your personal project to the S'mores repositiory)
4. Initialize a new Git repo with `git init`
5. Run `composer install`
6. Activate the theme. This will download the plugins that come with the theme
7. Update the plugins

   - If you received an error during the plugin update process, refresh the page and try again.

8. If the server you are running is at admin.dev, skip to step 10
9. Open gulpfile.js and change `proxy: "admin.dev"` to whatever URL your server runs at (wickersmith.wism.dev, for instance)
10. Run NPM Install



## Development

Oldfashioned uses [Grunt](http://gulpjs.com/) for compiling Sass to CSS, checking for JS errors, concatenating and minifying files, optimizing PNGs and JPEGs, versioning assets, and generating lean Modernizr builds.

### Branches

There are currently two branches. Master and Wordpress. Both contain the same dependency files and assets folder. 
1. `master` contains a single starter HTML files, for wireframes/templates


To switch branches type: `git checkout <branch name>` from the command line.

# Credits

##Findsome & Winmore

Oldfashioned is maintained and funded by [Findsome & Winmore](http://findsomewinmore.com). This open source project is brought to you by dozens of other open source projects. We like to give credit to those that we have borrowed from. If you find a code snippet we forgot to credit, please submit a pull request for a README.md update.

# License

Oldfashioned is Copyright © 2017 Findsome & Winmore. It is free software, and may be redistributed under the terms specified in the [LICENSE](LICENSE.md) file.
